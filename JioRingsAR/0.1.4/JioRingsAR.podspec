
Pod::Spec.new do |spec|

  spec.name         = "JioRingsAR"
  spec.version      = "0.1.4"
  spec.summary      = "Extension for integrating social distancing using Arkit"

  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.author = { "Tesseract" => "covidar@tesseract.in" }
  spec.homepage = "https://tesseract.in/"
  spec.source = { :git => "https://grappus_tesseract@bitbucket.org/tesseract-external/jiocovid19-ios.git",
                  :tag => "#{spec.version}" }
             
  spec.platform = :ios
  spec.ios.deployment_target = "10.0"
  
  spec.source_files = "JioRingsAR/**/*.{swift}"
  
  spec.framework = "UIKit"
  spec.dependency 'Alamofire', '~> 4.9.1'
  spec.resources = "JioRingsAR/**/*.{png,storyboard,xib,xcassets,mp3}"
  spec.swift_version = "5.0"

end
