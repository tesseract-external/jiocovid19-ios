//
//  Constants.swift
//  Jio-Covid
//

import Foundation
import UIKit

let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height

let baseApiUrl = "https://apps.tesseract.in/jio-covid19/api/v1"
let baseImageUrl = "https://jio-ar-dev.s3.ap-south-1.amazonaws.com/app-assets/icons/"
let ringsUrl = "https://jio-ar-dev.s3.ap-south-1.amazonaws.com/app-assets/coloredRings.usdz"
let modelUrl = "https://jio-ar-dev.s3.ap-south-1.amazonaws.com/app-assets/PoseNetMobileNet075S16FP16.mlmodel"

struct UserDefaultKeys {  
  static let kAuthToken = "kAuthToken"
  static let kUserSafety = "kUserSafety"
  static let kUserName = "kUserName"
  static let kUUID = "kUUID"
  static let kUserId = "kUserId"
  static let kVibrationEnabled = "kVibrationEnabled"
  static let kSoundEnabled = "kSoundEnabled"
  static let kSelectedSound = "kSelectedSound"
  static let kMLModelUrl = "kMLModelUrl"
  static let kLatitude = "kLatitude"
  static let kLongitude = "kLongitude"
}

enum SoundName: String {
  case spaceDrop = "Space Drop"
  case accent = "Accent"
  case alarm = "Alarm"
}

enum OnboardingImages: String {
  case walkthrough1 = "walkthrough1"
  case walkthrough2 = "walkthrough2"
  case camera = "camera"
  case location = "location"
  case notification = "notification"
  case jioLogo = "jioLogo"
  case connectedIcon = "connectedIcon"
  case goToSettings = "goToSettings"
}

enum HomeBtnImages: String {
  case homeSelected = "homeSelected"
  case homeUnselectedProfile = "homeUnselectedProfile"
  case homeUnselectedJournal = "homeUnselectedJournal"
  case journalSelected = "journalSelected"
  case journalUnselectedHome = "journalUnselectedHome"
  case journalUnselectedProfile = "journalUnselectedProfile"
  case profileSelected = "profileSelected"
  case profileUnselected = "profileUnselected"
}

enum StatsIcons: String {
  case walking = "walking"
  case warning = "warning"
  case time = "time"
  case timeTracked = "timeTracked"
  case totalViolations = "totalViolations"
  case safety = "safety"
  case safetyMaintained = "safetyMaintained"
  case sessionStats = "sessionStats"
}

enum MiscellaneousIcons: String {
  case vibrationToggle = "vibrationToggle"
  case soundToggle = "soundToggle"
  case notificationToggle = "notificationToggle"
  case rightCameraCorner = "rightCameraCorner"
  case leftCameraCorner = "leftCameraCorner"
  case dismissCovid = "dismissCovid"
  case dismissStats = "dismissStats"
  case arrow = "arrow"
}

struct DownloadImage {  
  var imageLocalUrl: URL?
  var imageRemoteUrl: String?
  var image: UIImage?
}

let urlStrings = [
  baseImageUrl + "walkthrough1%402x.png",
  baseImageUrl + "walkthrough2%402x.png",
  baseImageUrl + "camera%402x.png",
  baseImageUrl + "notification%402x.png",
  baseImageUrl + "location%402x.png",
  baseImageUrl + "jioLogo%402x.png",
  
  baseImageUrl + "arrow%402x.png",
  baseImageUrl + "connectedIcon%402x.png",
  baseImageUrl + "safety%402x.png",
  baseImageUrl + "sessionStats%402x.png",
  baseImageUrl + "notificationToggle%402x.png",
  baseImageUrl + "time%402x.png",
  baseImageUrl + "leftCameraCorner%402x.png",
  baseImageUrl + "goToSettings%402x.png",
  baseImageUrl + "soundToggle%402x.png",
  baseImageUrl + "rightCameraCorner%402x.png",
  baseImageUrl + "dismissStats%402x.png",
  baseImageUrl + "warning%402x.png",
  baseImageUrl + "vibrationToggle%402x.png",
  baseImageUrl + "home%402x.png",
  
  baseImageUrl + "profileSelected%402x.png",
  baseImageUrl + "journalSelected%402x.png",
  baseImageUrl + "homeSelected%402x.png",
  baseImageUrl + "journalUnselectedHome%402x.png",
  baseImageUrl + "journalUnselectedProfile%402x.png",
  baseImageUrl + "homeUnselectedJournal%402x.png",
  baseImageUrl + "homeUnselectedProfile%402x.png",
  baseImageUrl + "profileUnselected%402x.png",
  baseImageUrl + "dismissCovid%402x.png",
  baseImageUrl + "walking%402x.png",
  baseImageUrl + "safetyMaintained%402x.png",
  baseImageUrl + "totalViolations%402x.png",
  baseImageUrl + "timeTracked%402x.png"
]
