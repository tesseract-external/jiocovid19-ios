//
//  Extensions.swift
//  Jio-Covid
//

import CoreGraphics

extension CGImage {
    var size: CGSize {
        return CGSize(width: width, height: height)
    }
}
