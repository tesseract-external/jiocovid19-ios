//
//  Extensions.swift
//  Jio-Covid
//

import Foundation
import UIKit
import ARKit
import Alamofire

extension Date {
  
  static let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
  
  func days(from date: Date) -> Int {
    return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
  }
  
  func years(from date: Date) -> Int {
    return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
  }
}

@available(iOS 13.0, *)
extension UIView {
  func round(corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: bounds,
                            byRoundingCorners: corners,
                            cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
  }
  
  func showToast(toastMessage: String) {
    let toastRect = CGRect(x: self.frame.size.width/2 - 60,
                           y: 50,
                           width: 120,
                           height: 35)
    let toastLabel = UILabel(frame: toastRect)
    if let toastColor = UIColor(named: "ToastColor") {
      toastLabel.backgroundColor = toastColor
    }
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = .center
    toastLabel.text = toastMessage
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10
    toastLabel.clipsToBounds = true
    toastLabel.numberOfLines = 0
    toastLabel.frame = CGRect(x: 0,
                              y: toastLabel.frame.minY,
                              width: UIScreen.main.bounds.width,
                              height: toastLabel.frame.height*3 + 8)
    
    self.addSubview(toastLabel)
    UIView.animate(withDuration: 2.0,
                   delay: 0.1,
                   options: .curveEaseOut,
                   animations: {
                    toastLabel.alpha = 0.0
    }, completion: { _ in
      toastLabel.removeFromSuperview()
    })
  }
  
  @IBInspectable
  var cRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable
  var bWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var bColor: UIColor? {
    get {
      if let color = layer.borderColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.borderColor = color.cgColor
      } else {
        layer.borderColor = nil
      }
    }
  }
  
  @IBInspectable
  var sRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  @IBInspectable
  var sOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
  @IBInspectable
  var sOffset: CGSize {
    get {
      return layer.shadowOffset
    }
    set {
      layer.shadowOffset = newValue
    }
  }
  
  @IBInspectable
  var sColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set {
      if let color = newValue {
        layer.shadowColor = color.cgColor
      } else {
        layer.shadowColor = nil
      }
    }
  }
  
  func addGradient(colors: [CGColor], points: [CGPoint]) {
    let gradient = CAGradientLayer()
    
    gradient.colors = colors
    
    if let firstPoint = points.first,
      let secondPoint = points.last {
      gradient.startPoint = firstPoint
      gradient.endPoint = secondPoint
    }
    
    gradient.frame = self.bounds
    
    self.layer.addSublayer(gradient)
    self.layoutIfNeeded()
  }
  
  // ->1
  enum Direction: Int {
    case topToBottom = 0
    case bottomToTop
    case leftToRight
    case rightToLeft
  }
  
  func startShimmeringAnimation(animationSpeed: Float = 1.4,
                                direction: Direction = .leftToRight,
                                repeatCount: Float = MAXFLOAT) {
    
    // Create color  ->2
    let greyColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5).cgColor
    let whiteColor = UIColor.white.cgColor
    
    // Create a CAGradientLayer  ->3
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [greyColor, whiteColor, greyColor]
    gradientLayer.frame = CGRect(x: -self.bounds.size.width,
                                 y: -self.bounds.size.height,
                                 width: 3 * self.bounds.size.width,
                                 height: 3 * self.bounds.size.height)
    
    switch direction {
    case .topToBottom:
      gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
      gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
      
    case .bottomToTop:
      gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
      gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
      
    case .leftToRight:
      gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
      
    case .rightToLeft:
      gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
      gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
    }
    
    gradientLayer.locations =  [0.35, 0.50, 0.65] //[0.4, 0.6]
    self.layer.mask = gradientLayer
    
    // Add animation over gradient Layer  ->4
    CATransaction.begin()
    let animation = CABasicAnimation(keyPath: "locations")
    animation.fromValue = [0.0, 0.1, 0.2]
    animation.toValue = [0.8, 0.9, 1.0]
    animation.duration = CFTimeInterval(animationSpeed)
    animation.repeatCount = repeatCount
    CATransaction.setCompletionBlock { [weak self] in
      guard let strongSelf = self else { return }
      strongSelf.layer.mask = nil
    }
    gradientLayer.add(animation, forKey: "shimmerAnimation")
    CATransaction.commit()
  }
  
  func stopShimmeringAnimation() {
    self.layer.mask = nil
  }
  
  func configureAndStartShimmering() {
    
    backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.25)
    startShimmering()
  }
  
  func startShimmering() {
    
    let greyColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5).cgColor
    let whiteColor = UIColor.white.cgColor
    
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.colors = [greyColor, whiteColor, greyColor]
    gradient.frame = CGRect(x: -bounds.size.width, y: 0, width: 3 * bounds.size.width, height: bounds.size.height)
    
    gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradient.endPoint   = CGPoint(x: 1.0, y: 0.525)
    gradient.locations  = [0.4, 0.5, 0.6]
    
    layer.mask = gradient
    
    let animation: CABasicAnimation = CABasicAnimation.init(keyPath: "locations")
    animation.fromValue = [0.0, 0.1, 0.2]
    animation.toValue   = [0.8, 0.9, 1.0]
    
    animation.duration = 1.5
    animation.repeatCount = Float.greatestFiniteMagnitude
    animation.isRemovedOnCompletion = false
    
    gradient.add(animation, forKey: "shimmer")
  }
  
  func stopShimmering() {
    layer.mask = nil
  }
}

extension Collection {
  /// Returns the element at the specified index if it is within bounds, otherwise nil.
  subscript (safe index: Index) -> Element? {
    return indices.contains(index) ? self[index] : nil
  }
}

extension UILabel {
  func animate(newText: String, characterDelay: TimeInterval) {
    DispatchQueue.main.async {
      var textToDisplay = ""
      for (index, character) in newText.enumerated() {
        DispatchQueue.main.asyncAfter(deadline: .now() + characterDelay * Double(index)) {
          let font = UIFont(name: "NeueMontreal-Bold", size: 44.0)
          textToDisplay.append(character)
          let attributedString = NSMutableAttributedString(string: textToDisplay)
          
          let paragraphStyle = NSMutableParagraphStyle()
          paragraphStyle.lineHeightMultiple = 0.8
          
          attributedString.addAttributes([NSAttributedString.Key.font: font!,
                                          NSAttributedString.Key.paragraphStyle: paragraphStyle],
                                         range: NSRange(location: 0, length: attributedString.length))
          
          self.attributedText = attributedString
        }
      }
    }
  }
}

open class CustomLabel : UILabel {
  /*
   Handles Character Spacing in label text
   **/
  @IBInspectable open var characterSpacing:CGFloat = 1 {
    didSet {
      let attributedString = NSMutableAttributedString(string: self.text!)
      attributedString.addAttribute(.kern,
                                    value: self.characterSpacing,
                                    range: NSRange(location: 0, length: attributedString.length))
      self.attributedText = attributedString
    }
    
  }
}

open class CustomButton : UIButton {
  @IBInspectable open var characterSpacing: CGFloat = 1 {
    didSet {
      let attributedString = NSMutableAttributedString(string: self.titleLabel?.text ?? "")
      attributedString.addAttribute(.kern,
                                    value: self.characterSpacing,
                                    range: NSRange(location: 0,
                                                   length: attributedString.length))
      self.titleLabel?.attributedText = attributedString
    }
  }
}

extension String {
  func widthOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSAttributedString.Key.font: font]
    let size = self.size(withAttributes: fontAttributes)
    return size.width
  }
  
  func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect,
                                        options: .usesLineFragmentOrigin,
                                        attributes: [NSAttributedString.Key.font: font],
                                        context: nil)
    return ceil(boundingBox.width)
  }
}

extension UITextField {
  
  @IBInspectable var doneAccessory: Bool {
    get {
      return self.doneAccessory
    }
    set (hasDone) {
      if hasDone {
        addDoneButtonOnKeyboard()
      }
    }
  }
  
  func addDoneButtonOnKeyboard() {
    let frameToSet = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
    let doneToolbar: UIToolbar = UIToolbar(frame: frameToSet)
    doneToolbar.barStyle = .default
    
    let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                    target: nil,
                                    action: nil)
    let done: UIBarButtonItem = UIBarButtonItem(title: "Done",
                                                style: .done,
                                                target: self,
                                                action: #selector(self.doneButtonAction))
    done.tintColor = UIColor.white
    
    let items = [flexSpace, done]
    doneToolbar.items = items
    doneToolbar.sizeToFit()
    
    self.inputAccessoryView = doneToolbar
  }
  
  @objc func doneButtonAction() {
    self.resignFirstResponder()
  }
}

class GradientView: UIView {
    override open class var layerClass: AnyClass {
       return CAGradientLayer.classForCoder()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
      gradientLayer.colors = [UIColor.black.withAlphaComponent(0.5).cgColor,
                              UIColor.black.withAlphaComponent(0.0).cgColor,
                              UIColor.black.withAlphaComponent(0.0).cgColor,
                              UIColor.black.withAlphaComponent(0.0).cgColor,
                              UIColor.black.withAlphaComponent(0.7).cgColor]
      gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
      gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
    }
}

extension UIImageView {
  func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse,
              httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
  func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension Double {
  func truncate(places: Int) -> Double {
    return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
  }
}

extension Int64 {

    var ordinal: String {
        var suffix: String
        let ones: Int64 = self % 10
        let tens: Int64 = (self/10) % 10
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(self)\(suffix)"
    }

}

extension float4x4 {
  var translation: SIMD3<Float> {
    let translation = self.columns.3
    return SIMD3<Float>(translation.x, translation.y, translation.z)
  }
}

extension matrix_float4x4 {
  func position() -> SCNVector3 {
    return SCNVector3Make(columns.3.x, columns.3.y, columns.3.z)
  }
}

extension SCNVector3 {
  static func distanceFrom(vector vector1: SCNVector3, toVector vector2: SCNVector3) -> Float {
    let x0 = vector1.x
    let x1 = vector2.x
    let z0 = vector1.z
    let z1 = vector2.z
    
    return sqrtf(powf(x1-x0, 2) + powf(z1-z0, 2))
  }
}

extension FileManager {
  static var documentDirectoryURL: URL {
    let documentDirectoryURL = FileManager.default.urls(for: .applicationSupportDirectory,
                                                        in: .userDomainMask)[0]
    return documentDirectoryURL
  }
}

extension UIViewController {
  func loadImagefromDisk(imageName: String?) -> UIImage? {
    let imageURL = FileManager.documentDirectoryURL.appendingPathComponent("Images/\(imageName ?? "").png")
      do {        
        let imgData = try Data.init(contentsOf: imageURL)
        if let retrivedImg = UIImage.init(data: imgData) {
          return retrivedImg
        } else {
          return nil
        }
      } catch {
        print(error.localizedDescription)
        return nil
      }
  }
}

extension JSONDecoder {
  func decodeResponse<T: Decodable>(from response: DataResponse<Data>) -> Result<T> {
    guard response.error == nil else {
      print(response.error!)
      return .failure(response.error!)
    }

    guard let responseData = response.data else {
      print("didn't get any data from API")
      let error = NSError(domain: "", code: 400, userInfo: nil)
      return .failure(error)
    }

    do {
      let item = try decode(T.self, from: responseData)
      return .success(item)
    } catch {
      print("error trying to decode response")
      print(error)
      return .failure(error)
    }
  }
}
