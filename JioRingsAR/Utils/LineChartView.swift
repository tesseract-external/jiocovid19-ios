//
//  LineChartView.swift
//  Jio-Covid
//

import UIKit

class LineChartView : UIView {
  
  let graphLayer = CAShapeLayer()
  var points = [CGPoint]()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.layer.addSublayer(graphLayer)
    graphLayer.fillColor = UIColor.red.cgColor
    graphLayer.strokeColor = UIColor.red.cgColor
    graphLayer.backgroundColor = UIColor.clear.cgColor
    self.backgroundColor = UIColor.clear
    dynamicallyDrawCustomView(with: frame)
  }
  
  override func draw(_ rect: CGRect) {    
    dynamicallyDrawCustomView(with: rect)
    
    UIColor(red: 255/255, green: 0, blue: 0, alpha: 1).setFill()
    let size = CGSize(width: 4, height: 4)
    for origin in points {
      let pointRect = CGRect(origin: CGPoint(x: origin.x-2, y: origin.y-2),
                             size: size)
      let quad = UIBezierPath.init(roundedRect: pointRect,
                                   cornerRadius: 2)
      quad.fill()
      quad.stroke()
    }
    self.backgroundColor = UIColor.clear
  }
  
  func dynamicallyDrawCustomView(with rect:CGRect) {
    let context = UIGraphicsGetCurrentContext()
    UIColor(red: 255/255, green: 0, blue: 0, alpha: 1).set()
    let line = UIBezierPath()
    if points.count > 2 {
      line.move(to: points.first ?? CGPoint.zero)
      
      for count in 1..<points.count {
        line.addLine(to: points[count])
      }
      line.lineWidth = 2
      line.stroke()
    }
    graphLayer.path = line.cgPath
    if let context = context {
      graphLayer.draw(in: context)
    }
  }
}
