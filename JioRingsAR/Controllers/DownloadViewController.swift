//
//  ViewController.swift
//  Jio-Covid
//

import UIKit
import CoreML
import ARKit
import Alamofire

@available(iOS 13.0, *)
class DownloadViewController: UIViewController {
  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  var downloadedImages = [String: DownloadImage]()
  var newimgs = [UIImage]()
  var currentTaskNumber = 0
  var compiledModelName = ""
  var fetchedModel: MLModel?
  var ringscene: SCNScene?
  let downloadingGroup = DispatchGroup()
  var imageCount = 0  
  
  override func viewDidLoad() {
    super.viewDidLoad()    
    checkIfFilesExist()
  }
  
  func downloadRings() {
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
      let documentsURL = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)[0]
      let fileURL = documentsURL.appendingPathComponent("coloredRings.usdz")
      
      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    }
    
    downloadingGroup.enter()
    Alamofire.download(ringsUrl,
                to: destination)
      .response { response in
        self.downloadingGroup.leave()
        if response.error == nil {
          do {
            let fileURL = FileManager.documentDirectoryURL.appendingPathComponent("coloredRings.usdz")
            self.ringscene = try SCNScene(url: fileURL, options: nil)
          } catch {
            
          }
        }
    }
  }
  
  func checkIfFilesExist() {
    createDirectory()
    
    activityIndicator.startAnimating()
    let imageFilePath = FileManager.documentDirectoryURL.appendingPathComponent("Images")
    if !FileManager.default.fileExists(atPath: imageFilePath.path) {
      downloadImages()
    }
    let ringFilePath = FileManager.documentDirectoryURL.appendingPathComponent("coloredRings.usdz")
    if !FileManager.default.fileExists(atPath: ringFilePath.path) {
      downloadRings()
    }
    if UserDefaults.standard.value(forKey: UserDefaultKeys.kMLModelUrl) as? String == nil {
      if !ARBodyTrackingConfiguration.isSupported {
        downloadModel()
      }
    }
  
    downloadingGroup.notify(queue: .main, execute: {
      self.activityIndicator.stopAnimating()
      self.performSegue(withIdentifier: "showWalkthrough", sender: nil)
    })
  }
  
  func createDirectory() {
    let fileManager = FileManager.default
    let urls = fileManager.urls(for: .applicationSupportDirectory, in: .userDomainMask)
    if let applicationSupportURL = urls.last {
      do {
        try fileManager.createDirectory(at: applicationSupportURL, withIntermediateDirectories: true, attributes: nil)
      }
      catch {
        
      }
    }
  }
  
  func fetchModel() {
    let fileManager = FileManager.default
    let appSupportDirectory = try! fileManager.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    
    let newpermanentUrl = appSupportDirectory.appendingPathComponent(compiledModelName)
    
    let model = try? MLModel(contentsOf: newpermanentUrl)
    self.fetchedModel = model
  }
  
  func downloadModel() {
    guard let url = URL(string: modelUrl) else {
      return
    }
    downloadingGroup.enter()
    
    URLSession.shared.downloadTask(with: url) { (urlOrNil, response, error) in
      self.downloadingGroup.leave()
      if error != nil {
        return
      }
      
      guard let fileURL = urlOrNil else {
        return
      }
      do {
        //compiles the model
        let compiledModelURL = try MLModel.compileModel(at: fileURL)
        
        //turns the model into an MLModel
        let model = try MLModel(contentsOf: compiledModelURL)
        self.fetchedModel = model
        
        let fileManager = FileManager.default
        let appSupportURL = fileManager.urls(for: .applicationSupportDirectory,
                                             in: .userDomainMask).first!
        
        self.compiledModelName = compiledModelURL.lastPathComponent
        UserDefaults.standard.set(self.compiledModelName, forKey: UserDefaultKeys.kMLModelUrl)
        
        let permanentURL = appSupportURL.appendingPathComponent(self.compiledModelName)
        
        try fileManager.copyItem(at: compiledModelURL, to: permanentURL)
        
      } catch {
        
      }
    }.resume()
  }    
  
  func downloadImages() {
    downloadingGroup.enter()
    imageCount = urlStrings.count
    
    let queue = OperationQueue()
    queue.maxConcurrentOperationCount = 2
    
    for i in 0 ..< imageCount {
      let operation = NetworkOperation(urlString: urlStrings[i]) { responseObject, error, url in
        guard let responseObject = responseObject else {
          // handle error here
          self.currentTaskNumber += 1
          if self.currentTaskNumber == self.imageCount {
            self.saveImagesToDisk()
            self.downloadingGroup.leave()
          }
          return
        }
        
        self.currentTaskNumber += 1
        
        var imageName = ""
        // update UI to reflect the `responseObject` finished successfully
        if let urlComponents = url?.components(separatedBy: "icons/"), urlComponents.count == 2 {
          if let imageComponents = urlComponents.last?.components(separatedBy: "%402x.png"), imageComponents.count == 2 {
            imageName = imageComponents.first ?? ""
          }
        }
        
        if let imageData = responseObject as? Data {
          if let imageDownloaded = UIImage(data: imageData) {
            let imageObject = DownloadImage(imageLocalUrl: nil,
                                            imageRemoteUrl: url,
                                            image: imageDownloaded)
            self.downloadedImages[imageName] = imageObject
          }
        }
        if self.currentTaskNumber == self.imageCount {
          self.downloadingGroup.leave()
          self.saveImagesToDisk()
        }
      }
      queue.addOperation(operation)
    }
  }
  
  func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
  }
  
  func saveImagesToDisk() {
    //1. Access the Document Directory URL/Path
    let imgFolderURL = FileManager.documentDirectoryURL.appendingPathComponent("Images")
    
    if !FileManager.default.fileExists(atPath: imgFolderURL.path) {
      do {
        try FileManager.default.createDirectory(atPath: imgFolderURL.path,
                                                withIntermediateDirectories: true,
                                                attributes: nil)
      } catch {
        
      }
    }
    
    for (_, dictObject) in downloadedImages.enumerated() {
      if let imgData = dictObject.value.image?.pngData() {
        let imgURL = imgFolderURL.appendingPathComponent(dictObject.key).appendingPathExtension("png")
        
        do {
          try imgData.write(to: imgURL)
          var downloadObject = dictObject.value
          downloadObject.imageLocalUrl = imgURL
          self.downloadedImages[dictObject.key] = downloadObject
        }
        catch {
          
        }
      }
    }
  }
}
