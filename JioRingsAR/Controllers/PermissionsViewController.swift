//
//  PermissionsViewController.swift
//  Jio-Covid

import UIKit
import CoreLocation
import AVFoundation
import CoreML
import Alamofire

class PermissionsViewController: UIViewController, CLLocationManagerDelegate {
  
  // MARK: - IBOutlets
  @IBOutlet weak var proceedBtn: UIButton!  
  @IBOutlet var connectedIcons: [UIImageView]!  
  @IBOutlet weak var goToSettingsBtn: UIButton!
  @IBOutlet weak var cameraPopupView: UIView!  
  @IBOutlet weak var blurView: UIVisualEffectView!
  @IBOutlet weak var jioLogo: UIImageView!
  @IBOutlet weak var notificationImageView: UIImageView!
  @IBOutlet weak var locationImageView: UIImageView!  
  @IBOutlet weak var settingsImageView: UIImageView!
  @IBOutlet weak var cameraImageView: UIImageView!
  @IBOutlet weak var cameraPopupBgImageView: UIView!
  @IBOutlet weak var cameraPopupImageView: UIImageView!  
  @IBOutlet weak var dismissFlowBtn: UIButton!
  
  @IBOutlet weak var cameraPopupBottomConstraint: NSLayoutConstraint!
  
  // MARK: - Variables
  var locationManager: CLLocationManager?
  
  // MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    loadImages()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
    
    switch cameraAuthorizationStatus {
    case .authorized:
      self.connectedIcons[0].alpha = 1.0
      self.view.layoutIfNeeded()
      self.hideCameraPopup()
      self.moveToHome()
      
    default: break
    }
  }
  
  func loadImages() {
    jioLogo.image = self.loadImagefromDisk(imageName: OnboardingImages.jioLogo.rawValue)
    locationImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.location.rawValue)
    cameraImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.camera.rawValue)
    notificationImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.notification.rawValue)
    settingsImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.goToSettings.rawValue)
    cameraPopupImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.camera.rawValue)
    dismissFlowBtn.setImage(self.loadImagefromDisk(imageName: MiscellaneousIcons.dismissCovid.rawValue),
                            for: .normal)
    connectedIcons[0].image = self.loadImagefromDisk(imageName: OnboardingImages.connectedIcon.rawValue)
                              
    connectedIcons[1].image = self.loadImagefromDisk(imageName: OnboardingImages.connectedIcon.rawValue)
                               
    connectedIcons[2].image = self.loadImagefromDisk(imageName: OnboardingImages.connectedIcon.rawValue)
  }
  
  // MARK: - IBActions
  @IBAction func proceedBtnDidTap(_ sender: Any) {
    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
    
    switch cameraAuthorizationStatus {
    case .denied:
      self.showCameraRequiredPopup()
      
    case .authorized:
      self.moveToHome()
      
    case .notDetermined:
      self.requestCameraPermission()
      self.hideCameraPopup()
      
    default: break
    }
  }
  
  @IBAction func goToSettingsDidTap(_ sender: Any) {
    self.moveToSettings()
  }
  
  @IBAction func dismissCovidDidTap(_ sender: Any) {
    if let navVC = self.navigationController {
      navVC.dismiss(animated: true, completion: nil)
    }
  }
  
  // MARK: - Custom Methods
  func requestCameraPermission() {
    AVCaptureDevice.requestAccess(for: .video, completionHandler: { accessGranted in
      guard accessGranted == true else {
        DispatchQueue.main.async {
          self.showCameraRequiredPopup()
        }
        return
      }
      DispatchQueue.main.async {
        self.connectedIcons[0].alpha = 1.0
        self.view.layoutIfNeeded()
      }
      self.checkIfLocationPermissionGranted()
    })
  }
  
  func checkIfLocationPermissionGranted() {
    if CLLocationManager.locationServicesEnabled() {
        switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
              self.getLocationPermission()
            case .authorizedAlways, .authorizedWhenInUse:
              self.checkForNotificationPermission()
        default:
            break
        }
    }
  }
  
  func getLocationPermission() {
    DispatchQueue.main.async {
      self.locationManager = CLLocationManager()
      self.locationManager?.delegate = self
      self.locationManager?.requestWhenInUseAuthorization()
      self.locationManager?.startUpdatingLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager,
                       didChangeAuthorization status: CLAuthorizationStatus) {
    if status == .authorizedWhenInUse {
      self.connectedIcons[1].alpha = 1.0
      self.view.layoutIfNeeded()
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        if CLLocationManager.isRangingAvailable() {
          self.checkForNotificationPermission()
//          guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//          UserDefaults.standard.set(locValue.latitude.magnitude, forKey: UserDefaultKeys.kLatitude)
//          UserDefaults.standard.set(locValue.longitude.magnitude, forKey: UserDefaultKeys.kLongitude)
//          updateUser(lat: locValue.latitude.magnitude, lng: locValue.longitude.magnitude)
        }
      }
    }
  }
  
  func checkForNotificationPermission() {
    let current = UNUserNotificationCenter.current()

    current.getNotificationSettings(completionHandler: { (settings) in
        if settings.authorizationStatus == .notDetermined {
          self.getNotificationPermission()
        } else {
          self.moveToHome()
        }
    })
  }
  
  func getNotificationPermission() {
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
      
      if error != nil {
        return
      }
      
      if granted == true {
        DispatchQueue.main.async {
          self.connectedIcons[2].alpha = 1.0
          self.view.layoutIfNeeded()
        }
        self.moveToHome()
      }
    }
  }
  
  func moveToHome() {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: "showHome", sender: nil)
    }
  }
  
  func showCameraRequiredPopup() {
    self.cameraPopupBottomConstraint.constant = -24.0
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseInOut,
                   animations: {
                    self.cameraPopupView.alpha = 1.0
                    self.blurView.alpha = 1.0
                    self.cameraPopupBgImageView.alpha = 1.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func hideCameraPopup() {
    self.cameraPopupBottomConstraint.constant = -300.0
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseInOut,
                   animations: {
                    self.cameraPopupView.alpha = 0.0
                    self.blurView.alpha = 0.0
                    self.cameraPopupBgImageView.alpha = 0.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func moveToSettings() {
    DispatchQueue.main.async {
      guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
        return
      }
      
      if UIApplication.shared.canOpenURL(settingsUrl) {
        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
          
        })
      }
    }
  }
  
  func updateUser(lat: Double, lng: Double) {
    guard let userId = UserDefaults.standard.value(forKey: UserDefaultKeys.kUserId) as? String else {
      return
    }
    let urlString = baseApiUrl + "/users/\(userId)"
    
    let parameters: [String: Any] = ["lastActiveLocation": ["latitude": lat,
                                                            "longitude": lng]]
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
               method:.put,
               parameters: parameters,
               encoding: JSONEncoding.default,
               headers: ["Authorization": token])
      .responseData { response in
      let decoder = JSONDecoder()
      let userResult: Result<BaseJsonStruct<UserData>> = decoder.decodeResponse(from: response)
        print(response)
        guard let user = userResult.value else {
          return
        }
        print(user.data)
    }
  }
}
