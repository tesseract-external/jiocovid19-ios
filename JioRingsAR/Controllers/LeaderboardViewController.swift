//
//  LeaderboardViewController.swift
//  Jio-Covid

import UIKit
import Alamofire

class LeaderboardViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var userRankLabel: CustomLabel!
  @IBOutlet weak var userNameInitialLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var durationLabel: UILabel!  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var safetyPercentageLabel: UILabel!
  
  @IBOutlet weak var nearMeTableView: UITableView!
  @IBOutlet weak var localUserRankLabel: CustomLabel!
  @IBOutlet weak var localUserNameInitialLabel: UILabel!
  @IBOutlet weak var localUserNameLabel: UILabel!
  @IBOutlet weak var localDurationLabel: UILabel!
  @IBOutlet weak var localActivityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var localSafetyPercentageLabel: UILabel!
  
  @IBOutlet weak var localRankBtn: UIButton!
  @IBOutlet weak var globalRankBtn: UIButton!
  @IBOutlet weak var rankOptionView: UIView!
  @IBOutlet weak var globalView: UIView!
  @IBOutlet weak var localView: UIView!
  @IBOutlet weak var scroller: UIScrollView!
  
  // MARK: - Variables
  var globalLeaderboardUsers = [LeaderboardUser]()
  var localLeaderboardUsers = [LeaderboardUser]()
  var totalCount: Int64 = 0
  var pageNumber = 0
  var localTotalCount: Int64 = 0
  var localPageNumber = 0
  
  // MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.contentInset = UIEdgeInsets(top: 10,
                                               left: 0.0,
                                               bottom: 10.0,
                                               right: 0.0)
    
    setInitialData()
    getGlobalLeaderboardData(page: 0)
    getLocalLeaderboardData(page: 0)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupUI()
  }
  
  @IBAction func localRankDidTap(_ sender: Any) {
    self.scroller.scrollRectToVisible(localView.frame, animated: true)
    self.localRankBtn.backgroundColor = UIColor(red: 67.0/255.0,
                                                green: 82.0/255.0,
                                                blue: 254.0/255.0,
                                                alpha: 1.0)
    self.localRankBtn.setTitleColor(UIColor.white, for: .normal)
    self.globalRankBtn.backgroundColor = UIColor.clear
    self.globalRankBtn.setTitleColor(UIColor.black, for: .normal)
  }
  
  @IBAction func globalRankDidTap(_ sender: Any) {
    self.scroller.scrollRectToVisible(globalView.frame, animated: true)
    self.localRankBtn.backgroundColor = UIColor.clear
    self.localRankBtn.setTitleColor(UIColor.black, for: .normal)
    self.globalRankBtn.backgroundColor = UIColor(red: 67.0/255.0,
                                                 green: 82.0/255.0,
                                                 blue: 254.0/255.0,
                                                 alpha: 1.0)
    self.globalRankBtn.setTitleColor(UIColor.white, for: .normal)
    self.view.layoutIfNeeded()
  }
  
  func setInitialData() {
    let username = UserDefaults.standard.value(forKey: UserDefaultKeys.kUserName) as? String ?? ""
    let safety = UserDefaults.standard.value(forKey: UserDefaultKeys.kUserSafety) as? Double ?? 0.0
    
    self.userNameLabel.text = username.capitalized
    self.userNameInitialLabel.text = username.first?.uppercased()
    self.safetyPercentageLabel.text = "\(safety.truncate(places: 2))%"
    
    self.localUserNameLabel.text = username.capitalized
    self.localUserNameInitialLabel.text = username.first?.uppercased()
    self.localSafetyPercentageLabel.text = "\(safety.truncate(places: 2))%"
    
    getGlobalRank()
    getLocalRank()
  }
  
  func getGlobalRank() {
    let urlString = baseApiUrl + "/leaderboards/rank/global"
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let globalRankResult: Result<GlobalRank> = decoder.decodeResponse(from: response)
        guard let globalRank = globalRankResult.value else {
          return
        }
        self.userRankLabel.text = "\(globalRank.rank)"
    }
  }
  
  func getLocalRank() {
    let urlString = baseApiUrl + "/leaderboards/rank/nearme"
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let globalRankResult: Result<GlobalRank> = decoder.decodeResponse(from: response)
        guard let globalRank = globalRankResult.value else {
          return
        }
        self.localUserRankLabel.text = "\(globalRank.rank)"
    }
  }
  
  func setupUI() {
    if UserDefaults.standard.value(forKey: UserDefaultKeys.kLatitude) as? Double != nil,
      UserDefaults.standard.value(forKey: UserDefaultKeys.kLongitude) as? Double != nil {
      rankOptionView.alpha = 1.0
      self.scroller.scrollRectToVisible(localView.frame, animated: false)
      self.view.layoutIfNeeded()
    } else {
      self.scroller.scrollRectToVisible(globalView.frame, animated: false)
    }
  }
  
  func getGlobalLeaderboardData(page: Int) {
    self.activityIndicator.startAnimating()
    let urlString = baseApiUrl + "/leaderboards/global?page=\(page)"
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let leaderboardDataResult: Result<BaseJsonStruct<Leaderboard>> = decoder.decodeResponse(from: response)
        self.activityIndicator.stopAnimating()
        guard let leaderboardData = leaderboardDataResult.value else {
          return
        }
        self.totalCount = leaderboardData.data.totalCount
        if self.globalLeaderboardUsers.isEmpty {
          self.globalLeaderboardUsers = leaderboardData.data.leaderboardUsers
        } else {
          self.globalLeaderboardUsers.append(contentsOf: leaderboardData.data.leaderboardUsers)
        }
        self.tableView.reloadData()
    }
  }
  
  func getLocalLeaderboardData(page: Int) {
    self.localActivityIndicator.startAnimating()
    let urlString = baseApiUrl + "/leaderboards/nearme?page=\(page)"
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let leaderboardDataResult: Result<BaseJsonStruct<Leaderboard>> = decoder.decodeResponse(from: response)
        self.localActivityIndicator.stopAnimating()
        guard let leaderboardData = leaderboardDataResult.value else {
          return
        }
        self.localTotalCount = leaderboardData.data.totalCount
        if self.localLeaderboardUsers.isEmpty {
          self.localLeaderboardUsers = leaderboardData.data.leaderboardUsers
        } else {
          self.localLeaderboardUsers.append(contentsOf: leaderboardData.data.leaderboardUsers)
        }
        self.nearMeTableView.reloadData()
    }
  }
}

extension LeaderboardViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == nearMeTableView {
      return localLeaderboardUsers.count
    } else {
      return globalLeaderboardUsers.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == nearMeTableView {
      guard let cell = tableView
        .dequeueReusableCell(withIdentifier: "LeaderboardTableViewCell", for: indexPath)
        as? LeaderboardTableViewCell else {
          return UITableViewCell()
      }
      cell.setupUI(user: self.localLeaderboardUsers[indexPath.row],
                   index: indexPath.row)
      return cell
    } else {
      guard let cell = tableView
        .dequeueReusableCell(withIdentifier: "LeaderboardTableViewCell", for: indexPath)
        as? LeaderboardTableViewCell else {
          return UITableViewCell()
      }
      cell.setupUI(user: self.globalLeaderboardUsers[indexPath.row],
                   index: indexPath.row)
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView,
                 willDisplay cell: UITableViewCell,
                 forRowAt indexPath: IndexPath) {
    if tableView == nearMeTableView {
      if indexPath.row == localLeaderboardUsers.count - 4 { // 3rd Last item
        if localTotalCount > localLeaderboardUsers.count {
          localPageNumber += 1
          self.getLocalLeaderboardData(page: localPageNumber)
        }
      }
    } else {
      if indexPath.row == globalLeaderboardUsers.count - 4 { // 3rd Last item
        if totalCount > globalLeaderboardUsers.count {
          pageNumber += 1
          self.getGlobalLeaderboardData(page: pageNumber)
        }
      }
    }
  }
}


