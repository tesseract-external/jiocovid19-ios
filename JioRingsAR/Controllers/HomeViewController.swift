//
//  ViewController.swift
//  Jio-Covid

import UIKit
import ARKit
import SceneKit.ModelIO
import Alamofire
import AVFoundation
import CoreML
import CoreLocation

@available(iOS 13.0, *)
class HomeViewController: UIViewController, CLLocationManagerDelegate {
  
  // MARK: - IBOutlets
  @IBOutlet weak var sceneViewContainer: UIView!
  @IBOutlet weak var safetyLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var violationsLabel: UILabel!
  @IBOutlet weak var statsView: UIView!
  @IBOutlet weak var endSessionView: UIView!
  @IBOutlet weak var endSessionBtn: UIButton!  
  @IBOutlet weak var moveAwayView: UIView!  
  @IBOutlet weak var lowerGradientView: UIView!
  
  @IBOutlet weak var startSessionShadowView: UIView!
  @IBOutlet weak var startSessionBtn: UIButton!
  @IBOutlet weak var startSessionView: UIView!
  @IBOutlet weak var journalBtn: UIButton!
  @IBOutlet weak var profileBtn: UIButton!
  @IBOutlet weak var homeBtn: UIButton!
  @IBOutlet weak var sessionEndedDismissView: UIView!
  @IBOutlet weak var dismissStatsBtn: UIButton!
  @IBOutlet weak var sessionEndedView: UIView!  
  @IBOutlet weak var blurView: UIVisualEffectView!
  
  @IBOutlet weak var detectingInstructionView: UIView!
  @IBOutlet weak var dismissSessionBtn: UIButton!
  
  @IBOutlet weak var detectedImage: PoseImageView!
  @IBOutlet weak var arrowImageView: UIImageView!
  @IBOutlet weak var leaderboardContainerView: UIView!
  @IBOutlet weak var leaderboardResizeView: UIView!
  @IBOutlet weak var finalSafetyLabel: CustomLabel!
  @IBOutlet weak var totalViolationsLabel: CustomLabel!
  
  @IBOutlet weak var rankLabel: CustomLabel!
  @IBOutlet weak var totalTimeLabel: CustomLabel!
  
  // Images to load
  @IBOutlet weak var dismissFlowBtn: UIButton!
  @IBOutlet weak var dismissEndStatsBtn: UIButton!
  
  @IBOutlet weak var statsSafetyImageView: UIImageView!
  @IBOutlet weak var statsTimeImageView: UIImageView!
  @IBOutlet weak var statsWarningImageView: UIImageView!
  @IBOutlet weak var moveAwayWarningImageView: UIImageView!
  @IBOutlet weak var leftCameraCornerImageView: UIImageView!
  @IBOutlet weak var rightCameraCornerImageView: UIImageView!
  @IBOutlet weak var sessionStatsImageView: UIImageView!
  
  @IBOutlet weak var endStatsSafetyImageView: UIImageView!
  @IBOutlet weak var endStatsTimeImageView: UIImageView!
  @IBOutlet weak var endStatsWarningImageView: UIImageView!
  
  @IBOutlet weak var boxYLabel: UILabel!
  @IBOutlet weak var bodyYLabel: UILabel!
  @IBOutlet weak var isIntersectingLabel: UILabel!
  
  @IBOutlet weak var sessionEndedCenterYConstraint: NSLayoutConstraint!
  @IBOutlet weak var startSessionBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var statsViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var journalBtnCenterXConstraint: NSLayoutConstraint!
  @IBOutlet weak var profileBtnCenterXConstraint: NSLayoutConstraint!
  @IBOutlet weak var profileTrailingConstraint: NSLayoutConstraint!
  @IBOutlet weak var journalLeadingConstraint: NSLayoutConstraint!  
  @IBOutlet weak var leaderboardHeightConstraint: NSLayoutConstraint!
  
  // MARK: - Variables
  var sceneView: ARSCNView?
  var floorDistance: CGFloat = -1.3
  weak var referenceNode: SCNNode?
  weak var tempNode: SCNNode?
  weak var bodyBorderNode: SCNNode?
  weak var bodyNode: SCNNode?
  weak var currentbodyAnchor: ARBodyAnchor?
  var currentDistance: CGFloat = 0.0
  var isLeaderboardExpanded = false
  var isSessionRunning = false
  var violationSoundEffect: AVAudioPlayer?
  let safeGreen = UIColor(red: 114.0/255.0, green: 238.0/255.0, blue: 26.0/255.0, alpha: 1.0)
  let dangerRed = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
  var locationManager: CLLocationManager!
  
  // 2D detection variables
  private var poseNet = PoseNet()
  private var currentFrame: CGImage?
  private var algorithm: Algorithm = .multiple
  private var poseBuilderConfiguration = PoseBuilderConfiguration()
  var fetchedModel: MLModel?
  
  var screenRatioFactor: CGFloat = 0.0
  var detectedRect = UIView(frame: CGRect.zero)
  var ringRect = UIView(frame: CGRect.zero)
  var detectedJoints = [UIView(frame: CGRect.zero),
                        UIView(frame: CGRect.zero),
                        UIView(frame: CGRect.zero),
                        UIView(frame: CGRect.zero),
                        UIView(frame: CGRect.zero),
                        UIView(frame: CGRect.zero)]
  
  // Intersection values
  var isIntersecting = false
  var isRectRed = false
  var isViolationBeingChecked = false
  var isViolationDetected = false
  var violations = 0
  var violationTimer = Timer()
  let thresholdViolationTime = 3.0
  let violationCheckInterval = 0.5
  let delayAfterViolationDetected = 3.0
  
  // Safety Violations values
  var intervalForAllowedViolations = 60.0
  var minAllowedViolations = 1
  var damagePerViolation = 2
  var currentSafety = 100
  
  var sessionTime = 0.0
  var startTime = Date()
  private var timer = Timer()
  private let serialQueue = DispatchQueue(label: "com.aboveground.dispatchqueueml")
  
  // MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.addSubview(ringRect)
    sceneView = ARSCNView(frame: view.bounds)
    sceneViewContainer.addSubview(sceneView ?? ARSCNView())
    setupUI()
    //    handleLocation()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.configureSession()
  }
  
  // MARK: - Default Methods
  func setupUI() {
    journalLeadingConstraint.constant = -kScreenWidth
    profileTrailingConstraint.constant = -kScreenWidth
    profileBtnCenterXConstraint.constant = kScreenWidth/2.0 - 51.0
    journalBtnCenterXConstraint.constant = -kScreenWidth/2.0 + 51.0
    self.statsView.layer.borderColor = UIColor.red.cgColor
    self.view.layoutIfNeeded()
    self.loadImages()
  }
  
  func loadImages() {
    statsSafetyImageView.image = self.loadImagefromDisk(imageName: StatsIcons.safety.rawValue)
    statsWarningImageView.image = self.loadImagefromDisk(imageName: StatsIcons.warning.rawValue)
    statsTimeImageView.image = self.loadImagefromDisk(imageName: StatsIcons.time.rawValue)
    endStatsSafetyImageView.image = self.loadImagefromDisk(imageName: StatsIcons.safety.rawValue)
    endStatsWarningImageView.image = self.loadImagefromDisk(imageName: StatsIcons.warning.rawValue)
    endStatsTimeImageView.image = self.loadImagefromDisk(imageName: StatsIcons.time.rawValue)
    sessionStatsImageView.image = self.loadImagefromDisk(imageName: StatsIcons.sessionStats.rawValue)
    rightCameraCornerImageView.image = self.loadImagefromDisk(imageName: MiscellaneousIcons.rightCameraCorner.rawValue)
    leftCameraCornerImageView.image = self.loadImagefromDisk(imageName: MiscellaneousIcons.leftCameraCorner.rawValue)
    arrowImageView.image = self.loadImagefromDisk(imageName: MiscellaneousIcons.arrow.rawValue)
    dismissFlowBtn.setImage(self.loadImagefromDisk(imageName: MiscellaneousIcons.dismissCovid.rawValue),
                            for: .normal)
    dismissEndStatsBtn.setImage(self.loadImagefromDisk(imageName: MiscellaneousIcons.dismissStats.rawValue),
                                for: .normal)
    homeBtn.setImage(self.loadImagefromDisk(imageName: HomeBtnImages.homeSelected.rawValue),
                     for: .normal)
    journalBtn.setImage(self.loadImagefromDisk(imageName: HomeBtnImages.journalUnselectedHome.rawValue),
                        for: .normal)
    profileBtn.setImage(self.loadImagefromDisk(imageName: HomeBtnImages.profileUnselected.rawValue),
                        for: .normal)
  }
  
  // MARK: - IBActions
  @IBAction func startSessionDidTap(_ sender: Any) {
    self.startSession()
    self.startTime = Date()
    isSessionRunning = true
  }
  
  @IBAction func endSessionDidTap(_ sender: Any) {
    self.endSession()
    self.bringInSessionEndedView()
    isSessionRunning = false
  }
  
  @IBAction func dismissStatsDidTap(_ sender: Any) {
    self.resetToStartSession()
  }
  
  @IBAction func dismissCovidDidTap(_ sender: Any) {
    if let navVC = self.navigationController {
      navVC.dismiss(animated: true, completion: nil)
    }
  }
  
  @IBAction func homeBtnTapped(_ sender: Any) {
    journalLeadingConstraint.constant = -kScreenWidth
    profileTrailingConstraint.constant = -kScreenWidth
    profileBtnCenterXConstraint.constant = kScreenWidth/2.0 - 51.0
    journalBtnCenterXConstraint.constant = -kScreenWidth/2.0 + 51.0
    UIView.animate(withDuration: 0.3,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.journalBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.journalUnselectedHome.rawValue),
                                             for: .normal)
                    self.profileBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.profileUnselected.rawValue),
                                             for: .normal)
                    self.homeBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.homeSelected.rawValue),
                                          for: .normal)
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  @IBAction func profileBtnTapped(_ sender: Any) {
    journalLeadingConstraint.constant = -kScreenWidth
    profileTrailingConstraint.constant = 0.0
    profileBtnCenterXConstraint.constant = 76.0
    journalBtnCenterXConstraint.constant = -76.0
    UIView.animate(withDuration: 0.3,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.journalBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.journalUnselectedHome.rawValue),
                                             for: .normal)
                    self.profileBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.profileSelected.rawValue),
                                             for: .normal)
                    self.homeBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.homeUnselectedJournal.rawValue),
                                          for: .normal)
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  @IBAction func journalBtnTapped(_ sender: Any) {
    journalLeadingConstraint.constant = 0.0
    profileTrailingConstraint.constant = -kScreenWidth
    profileBtnCenterXConstraint.constant = 76.0
    journalBtnCenterXConstraint.constant = -76.0
    UIView.animate(withDuration: 0.3,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.journalBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.journalSelected.rawValue),
                                             for: .normal)
                    self.profileBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.profileUnselected.rawValue),
                                             for: .normal)
                    self.homeBtn.setImage(self.loadImagefromDisk(imageName:    HomeBtnImages.homeUnselectedJournal.rawValue),
                                          for: .normal)
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  @IBAction func resizeLeaderboardDidTap(_ sender: Any) {
    if isLeaderboardExpanded {
      self.leaderboardHeightConstraint.constant = 354.0
      self.startSessionBottomConstraint.constant = 0.0
      
      UIView.animate(withDuration: 0.3,
                     delay: 0.0,
                     options: .curveEaseInOut,
                     animations: {
                      self.startSessionView.alpha = 1.0
                      self.startSessionBtn.alpha = 1.0
                      self.startSessionShadowView.alpha = 1.0
                      
                      self.arrowImageView.transform = self.arrowImageView.transform.rotated(by: .pi)
                      self.view.layoutIfNeeded()
      }, completion: { _ in
        
      })
    } else {
      self.leaderboardHeightConstraint.constant = kScreenHeight - 140
      self.startSessionBottomConstraint.constant = -157.0
      
      UIView.animate(withDuration: 0.3,
                     delay: 0.0,
                     options: .curveEaseInOut,
                     animations: {
                      self.startSessionView.alpha = 0.0
                      self.startSessionBtn.alpha = 0.0
                      self.startSessionShadowView.alpha = 0.0
                      self.arrowImageView.transform = self.arrowImageView.transform.rotated(by: .pi)
                      self.view.layoutIfNeeded()
      }, completion: { _ in
        
      })
    }
    isLeaderboardExpanded = !isLeaderboardExpanded
  }
  
  // MARK: - Custom Methods
  func configureSession() {
    let configuration = ARWorldTrackingConfiguration()
    sceneView?.session.run(configuration)
  }
  
  func startSession() {
    sceneView?.session.pause()
    sceneView?.removeFromSuperview()
    sceneView = nil
    
    sceneView = ARSCNView(frame: view.bounds)
    sceneViewContainer.addSubview(sceneView ?? ARSCNView())
    
    DispatchQueue.main.async {
      self.configureLighting()
      self.setBodyConfig()
      self.hideStartSession()
      
      if self.referenceNode == nil {
        self.drawTorusAtOrigin(floorDist: self.floorDistance)
      }
    }
  }
  
  func endSession() {
    hitSessionDataRequest()
    timer.invalidate()
    violationTimer.invalidate()
    
    isViolationBeingChecked = false
    isViolationDetected = false
    
    detectedRect.frame = CGRect.zero
    detectedRect.removeFromSuperview()
    for joint in detectedJoints {
      joint.removeFromSuperview()
    }
    
    // Calculate final time
    if sessionTime < 60 {
      self.totalTimeLabel.text = "\(Int(sessionTime)) sec"
    } else if sessionTime < 3600 {
      let minutes = Int(sessionTime/60.0)
      let seconds = Int(sessionTime.truncatingRemainder(dividingBy: 60.0))
      self.totalTimeLabel.text = "\(minutes) m \(seconds) sec"
    } else {
      let minutes = Int((sessionTime.truncatingRemainder(dividingBy: 3600.0))/60.0)
      let hours = Int(sessionTime/3600.0)
      self.totalTimeLabel.text = "\(hours) hr \(minutes) m"
    }
    
    if violations == 1 {
      self.totalViolationsLabel.text = "\(violations) violation"
    } else {
      self.totalViolationsLabel.text = "\(violations) violations"
    }
    
    finalSafetyLabel.text = "\(currentSafety)%"
    
    self.view.layoutIfNeeded()
    
    // Reset to default values
    sessionTime = 0.0
    violations = 0
    currentSafety = 100
    
    DispatchQueue.main.async {
      self.sceneView?.scene.rootNode.enumerateChildNodes { (node, stop) in
        node.removeFromParentNode()
      }
      self.bodyNode = nil
      self.bodyBorderNode = nil
      self.referenceNode = nil
      self.detectedRect.removeFromSuperview()
    }
  }
  
  func hitSessionDataRequest() {
    let urlString = baseApiUrl + "/activity/"
    
    let utcStartTime = getUTCStartTimeString()
    
    var parameters: [String: Any] = [:]
    if let latitude = UserDefaults.standard.value(forKey: UserDefaultKeys.kLatitude) as? Double,
      let longitude = UserDefaults.standard.value(forKey: UserDefaultKeys.kLongitude) as? Double {
      parameters = ["startTime": utcStartTime,
                    "totalDuration": Int(sessionTime),
                    "violationCount": violations,
                    "safetyRate": currentSafety,
                    "location": ["latitude": latitude,
                                 "longitude": longitude]
      ]
    } else {
      parameters = ["startTime": utcStartTime,
                    "totalDuration": Int(sessionTime),
                    "violationCount": violations,
                    "safetyRate": currentSafety]
    }
    
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
    
    Alamofire.request(urlString,
               method:.post,
               parameters: parameters,
               encoding: JSONEncoding.default,
               headers: ["Authorization": token])
      .responseData { response in
      let decoder = JSONDecoder()
      let activityResult: Result<Activity> = decoder.decodeResponse(from: response)
        guard let activity = activityResult.value else {
          return
        }
        self.updateUserData(rank: activity.rank,
                            localRank: activity.nearMeRank,
                            safety: activity.score)
    }
  }
  
  func handleLocation() {
    locationManager = CLLocationManager()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    
    if CLLocationManager.locationServicesEnabled(){
      guard let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate else { return }
      UserDefaults.standard.set(locValue.latitude.magnitude, forKey: UserDefaultKeys.kLatitude)
      UserDefaults.standard.set(locValue.longitude.magnitude, forKey: UserDefaultKeys.kLongitude)
    }
  }
  
  func updateUserData(rank: Int64, localRank: Int64?, safety: Double) {
    DispatchQueue.main.async {
      if let leaderboardVC = self.children[0] as? LeaderboardViewController {
        leaderboardVC.safetyPercentageLabel.text = "\(safety.truncate(places: 2))%"
        leaderboardVC.userRankLabel.text = "\(rank)"
        leaderboardVC.localUserRankLabel.text = "\(localRank ?? 0)"
        leaderboardVC.localLeaderboardUsers = []
        leaderboardVC.globalLeaderboardUsers = []
        leaderboardVC.getGlobalLeaderboardData(page: 0)
        leaderboardVC.getLocalLeaderboardData(page: 0)
      }
      
      if let journalVC = self.children[1] as? JournalViewController {
        journalVC.refreshData()
      }
      
      UIView.animate(withDuration: 0.2, animations: {
        self.rankLabel.text = "You’re now at the \(rank.ordinal) position on the leaderboard!"
        self.rankLabel.alpha = 1.0
        self.view.layoutIfNeeded()
      })
    }
  }
  
  func getUTCStartTimeString() -> String {
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    var utcString = dateFormatter.string(from: startTime)
    utcString.append("Z")
    
    return utcString
  }
  
  func setWorldConfig() {
    let configuration = ARWorldTrackingConfiguration()
    sceneView?.session.run(configuration)
    sceneView?.session.delegate = self
  }
  
  func setBodyConfig() {
    self.timer = Timer.scheduledTimer(timeInterval: 0.7,
                                      target: self,
                                      selector: #selector(self.loopCoreMLUpdate),
                                      userInfo: nil,
                                      repeats: true)
    
    if ARBodyTrackingConfiguration.isSupported {
      let configuration = ARBodyTrackingConfiguration()
      configuration.isLightEstimationEnabled = true
      configuration.automaticSkeletonScaleEstimationEnabled = true
      configuration.frameSemantics.insert(.bodyDetection)
      sceneView?.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
      
      sceneView?.delegate = self
      sceneView?.session.delegate = self
    } else {
      self.setWorldConfig()
      self.fetchModel()
      poseNet.poseNetMLModel = self.fetchedModel
      poseNet.delegate = self
    }
  }
  
  func fetchModel() {
    var compiledModelName = ""
    if let modelUrl = UserDefaults.standard.value(forKey: UserDefaultKeys.kMLModelUrl) as? String {
      compiledModelName = modelUrl
    }
    let fileManager = FileManager.default
    let appSupportDirectory = try! fileManager.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    
    let newpermanentUrl = appSupportDirectory.appendingPathComponent(compiledModelName)
    
    let model = try? MLModel(contentsOf: newpermanentUrl)
    self.fetchedModel = model
  }
  
  func configureLighting() {
    sceneView?.autoenablesDefaultLighting = true
    sceneView?.automaticallyUpdatesLighting = true
  }
  
  func hideStartSession() {
    self.startSessionBottomConstraint.constant = -157.0
    UIView.animate(withDuration: 0.4,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.startSessionView.alpha = 0.0
                    self.startSessionBtn.alpha = 0.0
                    self.startSessionShadowView.alpha = 0.0
                    self.leaderboardContainerView.alpha = 0.0
                    self.leaderboardResizeView.alpha = 0.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func fadeOutInstructions() {
    UIView.animate(withDuration: 0.5,
                   delay: 3.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.detectingInstructionView.alpha = 0.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func bringInStartSession() {
    self.detectingInstructionView.layer.removeAllAnimations()
    self.view.layoutIfNeeded()
    self.startSessionBottomConstraint.constant = 0.0
    UIView.animate(withDuration: 0.3,
                   delay: 0,
                   options: .curveEaseInOut,
                   animations: {
                    self.dismissSessionBtn.alpha = 0.0
                    self.detectingInstructionView.alpha = 0.0
                    self.startSessionView.alpha = 1.0
                    self.startSessionShadowView.alpha = 1.0
                    self.startSessionBtn.alpha = 1.0
                    self.leaderboardContainerView.alpha = 1.0
                    self.leaderboardResizeView.alpha = 1.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      self.endSession()
    })
  }
  
  // Using tap gesture get the position of floor
  @objc func addRingsToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
    let tapLocation = recognizer.location(in: sceneView)
    let hitTestResults = sceneView?.hitTest(tapLocation, types: .existingPlaneUsingExtent)
    
    guard let hitTestResult = hitTestResults?.first else { return }
    let translation = hitTestResult.worldTransform.translation
    let y = translation.y
    
    // Draw rings after getting the distance to floor
    if referenceNode == nil {
      floorDistance = CGFloat(y)
      drawTorusAtOrigin(floorDist: floorDistance)
    }
  }
  
  // Draw rings
  func drawTorusAtOrigin(floorDist: CGFloat) {
    let fileURL = FileManager.documentDirectoryURL.appendingPathComponent("coloredRings.usdz")
    let ringScene = try! SCNScene(url: fileURL, options: nil)
    
    referenceNode = ringScene.rootNode
    referenceNode?.position = SCNVector3(0, floorDist*100, 0)
    sceneView?.scene.rootNode.addChildNode(referenceNode ?? SCNNode())
    
    let box = SCNBox(width: 0.2, height: 0.2, length: 0.2, chamferRadius: 0)
    tempNode = SCNNode(geometry: box)
    tempNode?.position = SCNVector3(0, 0, 0)
    tempNode?.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
    sceneView?.scene.rootNode.addChildNode(tempNode ?? SCNNode())
    
    DispatchQueue.main.async {
      self.bringInStats()
    }
  }
  
  func bringInStats() {
    self.statsViewTopConstraint.constant = 21.0
    UIView.animate(withDuration: 0.5,
                   delay: 1.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.dismissSessionBtn.alpha = 0.0
                    self.detectingInstructionView.alpha = 0.0
                    self.statsView.alpha = 1.0
                    self.endSessionView.alpha = 1.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func bringInSessionEndedView() {
    self.statsViewTopConstraint.constant = -20.0
    self.sessionEndedCenterYConstraint.constant = 0.0
    
    UIView.animate(withDuration: 0.5,
                   delay: 0.5,
                   options: .curveEaseInOut,
                   animations: {
                    self.blurView.alpha = 1.0
                    self.sessionEndedView.alpha = 1.0
                    self.sessionEndedDismissView.alpha = 1.0
                    self.statsView.alpha = 0.0
                    self.endSessionView.alpha = 0.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func resetToStartSession() {
    self.startSessionBottomConstraint.constant = 0.0
    self.sessionEndedCenterYConstraint.constant = -100.0
    
    UIView.animate(withDuration: 0.3,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.blurView.alpha = 0.0
                    self.sessionEndedView.alpha = 0.0
                    self.sessionEndedDismissView.alpha = 0.0
                    self.startSessionView.alpha = 1.0
                    self.startSessionBtn.alpha = 1.0
                    self.startSessionShadowView.alpha = 1.0
                    self.leaderboardContainerView.alpha = 1.0
                    self.leaderboardResizeView.alpha = 1.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      self.sessionEndedCenterYConstraint.constant = 100.0
      self.safetyLabel.text = "100%"
      self.violationsLabel.text = "0 violations"
      self.timeLabel.text = "0 sec"
      self.view.layoutIfNeeded()
    })
  }
  
  func drawRect(joints: [Joint.Name: Joint]) {
    if joints.count == 0 {
      self.detectedRect.removeFromSuperview()
    } else {
      if detectedRect.superview == nil {
        self.detectedRect.frame = CGRect.zero
        self.view.addSubview(detectedRect)
      }
    }
    
    guard let leftShoulder = joints[Joint.Name.leftShoulder],
      let rightShoulder = joints[Joint.Name.rightShoulder],
      let leftAnkle = joints[Joint.Name.leftAnkle],
      let rightAnkle = joints[Joint.Name.rightAnkle],
      let leftHip = joints[Joint.Name.leftHip],
      let rightHip = joints[Joint.Name.rightHip] else {
        return
    }
    
    var reqdJoints = [Joint]()
    if leftAnkle.position == CGPoint.zero && rightAnkle.position == CGPoint.zero {
      let correctedLeftHipPos = leftHip.position.y + (leftHip.position.y - leftShoulder.position.y)
      leftHip.position = CGPoint(x: leftHip.position.x, y: correctedLeftHipPos)
      let correctedRightHipPos = rightHip.position.y + (rightHip.position.y - rightShoulder.position.y)
      rightHip.position = CGPoint(x: rightHip.position.x, y: correctedRightHipPos)
      reqdJoints = [leftShoulder, rightShoulder, leftHip, rightHip]
    } else {
      reqdJoints = [leftShoulder, rightShoulder, leftAnkle, rightAnkle]
    }
    
    let xPositions = getAllXPos(joints: reqdJoints)
    let yPositions = getAllYPos(joints: reqdJoints)
    
    // To check we have atleast 3 joints to create a rect
    if !(xPositions.count >= 3 && yPositions.count >= 3) {
      self.detectedRect.removeFromSuperview()
    }
    
    let xPos: CGFloat = xPositions.min() ?? 0.0
    let yPos: CGFloat = yPositions.min() ?? 0.0
    
    let width: CGFloat = (xPositions.max() ?? 0.0) - xPos
    let height: CGFloat = (yPositions.max() ?? 0.0) - yPos
    
    let rectFormed = CGRect(x: xPos,
                            y: yPos,
                            width: abs(width),
                            height: abs(height))
    self.detectedRect.frame = rectFormed
    if isIntersecting {
      self.detectedRect.backgroundColor = dangerRed.withAlphaComponent(0.5)
      self.detectedRect.layer.borderColor = dangerRed.cgColor
    } else {
      self.detectedRect.backgroundColor = UIColor.white.withAlphaComponent(0.5)
      self.detectedRect.layer.borderColor = safeGreen.cgColor
    }
    isRectRed = isIntersecting
    self.detectedRect.layer.borderWidth = 1.0
  }
  
  func getAllXPos(joints: [Joint]) -> [CGFloat] {
    var xPos = [CGFloat]()
    for joint in joints {
      if joint.position.x == 0.0 {
        continue
      } else {
        xPos.append(joint.position.x/screenRatioFactor)
      }
      
    }
    return xPos
  }
  
  func getAllYPos(joints: [Joint]) -> [CGFloat] {
    var yPos = [CGFloat]()
    for joint in joints {
      if joint.position.y == 0.0 {
        continue
      } else {
        yPos.append(joint.position.y/screenRatioFactor)
      }      
    }
    return yPos
  }
  
  @objc private func loopCoreMLUpdate() {
    sessionTime += 0.7
    calculateTime()
    
    if !ARBodyTrackingConfiguration.isSupported {
      serialQueue.async {
        self.updateCoreML()
      }
    }
  }
  
  func calculateTime() {
    if sessionTime < 60 {
      self.timeLabel.text = "\(Int(sessionTime)) sec"
    } else if sessionTime < 3600 {
      let minutes = Int(sessionTime/60.0)
      let seconds = Int(sessionTime.truncatingRemainder(dividingBy: 60.0))
      self.timeLabel.text = "\(minutes) m \(seconds) sec"
    } else {
      let minutes = Int((sessionTime.truncatingRemainder(dividingBy: 3600.0))/60.0)
      let hours = Int(sessionTime/3600.0)
      self.timeLabel.text = "\(hours) hr \(minutes) m"
    }
  }
  
  func updateCoreML() {
    let imageFromArkitScene = sceneView?.snapshot()
    
    DispatchQueue.main.async {
      let lowResImage = imageFromArkitScene?.resizeResolution(factor: 0.06)
      guard let cgimage = lowResImage?.cgImage else {
        fatalError("Captured image is null")
      }

      self.currentFrame = cgimage
      if self.isSessionRunning {
        self.poseNet.predict(cgimage)
      }
    }
  }
  
  func checkViolation() {
    if isRectRed {
      handleNewViolation()
      isRectRed = false
    }
  }
  
  // If intersection is present at threshold time, increment violations
  // start ignoring new violations
  func handleNewViolation() {
    violations+=1
    if violations == 1 {
      self.violationsLabel.text = "\(violations) violation"
    } else {
      self.violationsLabel.text = "\(violations) violations"
    }
    isViolationBeingChecked = false
    isViolationDetected = true
    startIgnoringNewViolations()
    calculateSafety()
    
    // Add vibration
    if let isVibrationEnabled = UserDefaults
      .standard.value(forKey: UserDefaultKeys.kVibrationEnabled) as? Bool {
      if isVibrationEnabled {
        handleVibration()
      }
    }
    
    // Add Sound
    if let isSoundEnabled = UserDefaults
      .standard.value(forKey: UserDefaultKeys.kSoundEnabled) as? Bool {
      if isSoundEnabled {
        playSound()
      }
    }
    // Animate Safety Banner
    animateMoveAwayBanner()
  }
  
  func animateMoveAwayBanner() {
    UIView.animate(withDuration: 0.1,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.moveAwayView.alpha = 1.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      self.statsView.layer.borderWidth = 2.0
      self.view.layoutIfNeeded()
      self.fadeOutMoveAwayBanner()
    })
  }
  
  func fadeOutMoveAwayBanner() {
    UIView.animate(withDuration: 0.1,
                   delay: 2.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.moveAwayView.alpha = 0.0
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      self.statsView.layer.borderWidth = 0.0
      self.view.layoutIfNeeded()
    })
  }
  
  func handleVibration() {
    let vibrationSupport = UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int ?? 0
    if vibrationSupport == 2 {
      let generator = UINotificationFeedbackGenerator()
      generator.notificationOccurred(.success)
    } else {
      AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
  }
  
  func playSound() {
    let bundle = Bundle(for: type(of:self))
    let selectedSound = UserDefaults.standard.value(forKey: UserDefaultKeys.kSelectedSound) as? String ?? ""
    guard let url = bundle.url(forResource: selectedSound, withExtension: "mp3") else {
      return
    }
    
    do {
      violationSoundEffect = try AVAudioPlayer(contentsOf: url)
      violationSoundEffect?.play()
    } catch {
      // couldn't load file :(
    }
  }
  
  func calculateSafety() {
    //    let allowedViolations = Int(sessionTime/intervalForAllowedViolations)
    if currentSafety > 40 {
      currentSafety = 100 - violations*damagePerViolation
    } else {
      currentSafety = 40
    }
    safetyLabel.text = "\(currentSafety)%"
  }
  
  // Start a timer to ignore new violations after one violation is detected
  func startIgnoringNewViolations() {
    violationTimer = Timer.scheduledTimer(timeInterval: delayAfterViolationDetected,
                                          target: self,
                                          selector: #selector(restartViolationCheck),
                                          userInfo: nil,
                                          repeats: false)
  }
  
  // Begin checking violations after delayed time
  @objc func restartViolationCheck() {
    isViolationDetected = false
  }
}

@available(iOS 13.0, *)
extension HomeViewController: ARSessionDelegate {
  func session(_ session: ARSession, didUpdate frame: ARFrame) {
    // For moving rings as the camera moves
    guard let camera = sceneView?.pointOfView else { return }
    referenceNode?.position = SCNVector3(x: camera.position.x,
                                         y: Float(floorDistance*100),
                                         z: camera.position.z)
    
    // Check if distance is less than 2m
    if ARBodyTrackingConfiguration.isSupported {
      if let torusPos = referenceNode?.worldPosition,
        let bodyPos = bodyNode?.worldPosition {
        let distance = SCNVector3.distanceFrom(vector: torusPos, toVector: bodyPos)
        currentDistance = CGFloat(distance)
        if distance < 2 {
          self.isIntersecting = true
        } else {
          self.isIntersecting = false
        }
        
        var isBodyVisible = false
        if let bodyNode = self.bodyNode,
          let cameraViewNode = sceneView?.pointOfView {
          isBodyVisible = sceneView?.isNode(bodyNode,
                                            insideFrustumOf: cameraViewNode) ?? false
        }
        
        if !isViolationDetected && isIntersecting && isBodyVisible {
          checkViolation()
        }
        self.view.layoutIfNeeded()
      }
    } else {
      if let pos = referenceNode?.position {
        let dir = calculateCameraDirection(cameraNode: camera)
        var edgePoint = pointInFrontOfPoint(point: pos, direction:dir, distance: 2.3)
        edgePoint.y = Float(floorDistance)
        tempNode?.position = edgePoint
        
        let projectedOrigin = self.sceneView?.projectPoint(edgePoint)
        
        self.boxYLabel.text = "Box y: \(projectedOrigin?.y ?? 0)"
        self.bodyYLabel.text = "Body y: \(self.detectedRect.frame.maxY)"
        
        var ringEdgeY = projectedOrigin?.y ?? 0
        if ringEdgeY == 0 {
          ringEdgeY = Float(kScreenHeight - 200)
        }
        
        if (CGFloat(self.detectedRect.frame.maxY) > CGFloat(ringEdgeY)) && detectedRect.superview != nil {
          self.isIntersecting = true
        } else {
          self.isIntersecting = false
        }
        
        self.isIntersectingLabel.text = "Merging: \(self.isIntersecting)"
        
        // Check if there is an existing violation being checked
        // or is there a new violation recently detected
        // and lastly check for intersection in order to check for new violations
        if !isViolationDetected && isIntersecting {
          checkViolation()
        }
      }
    }
  }
  
  func pointInFrontOfPoint(point: SCNVector3, direction: SCNVector3, distance: Float) -> SCNVector3 {
    var x = Float()
    var y = Float()
    var z = Float()
    x = point.x + distance * direction.x
    y = point.y + distance * direction.y
    z = point.z + distance * direction.z
    let result = SCNVector3Make(x, y, z)
    return result
  }
  
  func calculateCameraDirection(cameraNode: SCNNode) -> SCNVector3 {
    let x = -cameraNode.rotation.x
    let y = -cameraNode.rotation.y
    let z = -cameraNode.rotation.z
    let w = cameraNode.rotation.w
    let cameraRotationMatrix = GLKMatrix3Make(cos(w) + pow(x, 2) * (1 - cos(w)),
                                              x * y * (1 - cos(w)) - z * sin(w),
                                              x * z * (1 - cos(w)) + y*sin(w),
                                              y*x*(1-cos(w)) + z*sin(w),
                                              cos(w) + pow(y, 2) * (1 - cos(w)),
                                              y*z*(1-cos(w)) - x*sin(w),
                                              z*x*(1 - cos(w)) - y*sin(w),
                                              z*y*(1 - cos(w)) + x*sin(w),
                                              cos(w) + pow(z, 2) * ( 1 - cos(w)))
    let cameraDirection = GLKMatrix3MultiplyVector3(cameraRotationMatrix, GLKVector3Make(0.0, 0.0, -1.0))
    return SCNVector3FromGLKVector3(cameraDirection)
  }
}

@available(iOS 13.0, *)
extension HomeViewController: ARSCNViewDelegate {
  func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    // Detect if body node is added
    if let bodyAnchor = anchor as? ARBodyAnchor {
      currentbodyAnchor = bodyAnchor
      
      let rootPosition = bodyAnchor.skeleton.jointLocalTransforms.first?.position()
      
      let footIndex = bodyAnchor.skeleton.definition.index(for: .leftFoot)
      let footTransform = bodyAnchor.skeleton.definition.neutralBodySkeleton3D!.jointModelTransforms[footIndex]
      let distanceOfFootFromHipOnY = abs(footTransform.columns.3.y)
      
      let headIndex = bodyAnchor.skeleton.definition.index(for: .head)
      let headTransform = bodyAnchor.skeleton.definition.neutralBodySkeleton3D!.jointModelTransforms[headIndex]
      let distanceOfHeadFromHipOnY = abs(headTransform.columns.3.y)
      
      var width: CGFloat = 0.1
      if let leftShoulderPos = bodyAnchor.skeleton.modelTransform(for: .leftShoulder),
        let rightShoulderPos = bodyAnchor.skeleton.modelTransform(for: .rightShoulder) {
        width = CGFloat(3*(abs(leftShoulderPos.position().x) + abs(rightShoulderPos.position().x)))
      }
      
      let height = CGFloat(distanceOfFootFromHipOnY + distanceOfHeadFromHipOnY) + 0.3
      
      let surroundingCuboid = SCNBox(width: width,
                                     height: height,
                                     length: 0.5,
                                     chamferRadius: 0.0)
      let sm = "float u = _surface.diffuseTexcoord.x; \n" +
        "float v = _surface.diffuseTexcoord.y; \n" +
        "int u100 = int(u * 100); \n" +
        "int v100 = int(v * 100); \n" +
        "if (u100 % 99 == 0 || v100 % 99 == 0) { \n" +
        "  // do nothing \n" +
        "} else { \n" +
        "    discard_fragment(); \n" +
      "} \n"
      
      surroundingCuboid.materials.first?.shaderModifiers = [SCNShaderModifierEntryPoint.surface: sm]
      if currentDistance > 0.0 && currentDistance < 2.0 {
        surroundingCuboid.materials.first?.diffuse.contents = dangerRed
        surroundingCuboid.materials.first?.emission.contents = dangerRed
      } else {
        surroundingCuboid.materials.first?.diffuse.contents = safeGreen
        surroundingCuboid.materials.first?.emission.contents = safeGreen
      }
      
      surroundingCuboid.materials.first?.locksAmbientWithDiffuse = true
      
      let surroundingFillCuboid = SCNBox(width: width, height: height, length: width, chamferRadius: 0.0)
      if currentDistance > 0.0 && currentDistance < 2.0 {
        surroundingFillCuboid.materials.first?.diffuse.contents = dangerRed.withAlphaComponent(00.5)
      } else {
        surroundingFillCuboid.materials.first?.diffuse.contents = UIColor.white.withAlphaComponent(0.5)
      }
      
      surroundingFillCuboid.materials.first?.locksAmbientWithDiffuse = true
      
      let borderNode = SCNNode(geometry: surroundingCuboid)
      borderNode.position = rootPosition ?? SCNVector3(0, 0, 0)
      node.addChildNode(borderNode)
      bodyBorderNode = borderNode
      
      let borderFillNode = SCNNode(geometry: surroundingFillCuboid)
      borderFillNode.position = rootPosition ?? SCNVector3(0, 0, 0)
      node.addChildNode(borderFillNode)
      bodyNode = borderFillNode
    }
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    // Update body node position
    if let bodyAnchor = anchor as? ARBodyAnchor,
      let borderFillNode = node.childNodes.first {
      let position = bodyAnchor.skeleton.jointLocalTransforms.first?.position()
      borderFillNode.position = position ?? SCNVector3()
      bodyNode?.position = position ?? SCNVector3()
      
      if let surroundingFillCuboid = borderFillNode.geometry as? SCNBox {
        if currentDistance > 0.0 && currentDistance < 2.0 {
          surroundingFillCuboid.materials.first?.diffuse.contents = dangerRed
          surroundingFillCuboid.materials.first?.emission.contents = dangerRed
          isRectRed = true
        } else {
          surroundingFillCuboid.materials.first?.diffuse.contents = safeGreen
          surroundingFillCuboid.materials.first?.emission.contents = safeGreen
        }
      }
      
      
      if node.childNodes.count > 1 {
        let borderNode = node.childNodes[1]
        borderNode.position = position ?? SCNVector3()
        if let surroundingFillCuboid = borderNode.geometry as? SCNBox {
          if currentDistance > 0.0 && currentDistance < 2.0 {
            surroundingFillCuboid.materials.first?.diffuse.contents = dangerRed.withAlphaComponent(0.5)
          } else {
            surroundingFillCuboid.materials.first?.diffuse.contents = UIColor.white.withAlphaComponent(0.5)
          }
        }
        
      }
      bodyBorderNode?.position = position ?? SCNVector3()
    }
  }
}

@available(iOS 13.0, *)
extension HomeViewController: PoseNetDelegate {
  func poseNet(_ poseNet: PoseNet, didPredict predictions: PoseNetOutput) {
    defer {
      // Release `currentFrame` when exiting this method.
      self.currentFrame = nil
    }
    
    guard let currentFrame = currentFrame else {
      return
    }
    
    let poseBuilder = PoseBuilder(output: predictions,
                                  configuration: poseBuilderConfiguration,
                                  inputImage: currentFrame)
    
    screenRatioFactor = currentFrame.size.width/kScreenWidth
    
    let poses = algorithm == .single
      ? [poseBuilder.pose]
      : poseBuilder.poses
    
    drawRect(joints: poses.first?.joints ?? [:])
  }
}

extension UIImage {
  func resizeResolution(factor: CGFloat) -> UIImage? {
    let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width*factor, height: CGFloat(ceil(factor * size.height)))))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
    UIGraphicsEndImageContext()
    return result
  }
}
