//
//  PreferencesViewController.swift
//  Jio-Covid

import UIKit

class PreferencesViewController: UIViewController {

  // MARK: - IBOutlets
  @IBOutlet weak var userNameInitialLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var vibrationToggle: CustomSwitch!
  @IBOutlet var soundBtns: [UIButton]!
  @IBOutlet var soundTextLabels: [UILabel]!
  @IBOutlet weak var soundToggle: CustomSwitch!
  
  @IBOutlet weak var vibrationToggleImageView: UIImageView!
  @IBOutlet weak var soundToggleImageView: UIImageView!  
  @IBOutlet weak var selectedSongViewLeadingConstraint: NSLayoutConstraint!
  
  // MARK: - Lifecycle Methodss
  override func viewDidLoad() {
    super.viewDidLoad()
    setData()
    loadImages()
    soundChanged(soundBtns[0])
  }
  
  @IBAction func vibrationToggled(_ sender: Any) {
    UserDefaults.standard.set(vibrationToggle.isOn,
                              forKey: UserDefaultKeys.kVibrationEnabled)
  }
  
  @IBAction func soundToggled(_ sender: Any) {
    UserDefaults.standard.set(soundToggle.isOn,
                              forKey: UserDefaultKeys.kSoundEnabled)
  }
  
  @IBAction func soundChanged(_ sender: UIButton) {
    let tag = sender.tag
    var selectedsound = ""
    switch tag {
    case 1:
      selectedsound = SoundName.accent.rawValue
    case 2:
      selectedsound = SoundName.alarm.rawValue
    default:
      selectedsound = SoundName.spaceDrop.rawValue
    }
    UserDefaults.standard.set(selectedsound,
                              forKey: UserDefaultKeys.kSelectedSound)
    
    selectedSongViewLeadingConstraint.constant = CGFloat(tag)*(soundBtns.first?.frame.size.width ?? 0.0)
    UIView.animate(withDuration: 0.3,
                   delay: 0.0,
                   options: .curveEaseInOut,
                   animations: {
                    self.setAllSoundLabelsBlack()
                    self.soundTextLabels[tag].textColor = UIColor.white
                    self.view.layoutIfNeeded()
    }, completion: { _ in
      
    })
  }
  
  func setAllSoundLabelsBlack() {
    for label in soundTextLabels {
      label.textColor = UIColor.black
    }
  }
  
  func setData() {
    let username = UserDefaults.standard.value(forKey: UserDefaultKeys.kUserName) as? String ?? ""
    self.userNameLabel.text = username.capitalized
    self.userNameInitialLabel.text = username.first?.uppercased()
    
    if let isVibrationEnabled = UserDefaults
      .standard.value(forKey: UserDefaultKeys.kVibrationEnabled) as? Bool {
      vibrationToggle.isOn = isVibrationEnabled
    } else {
      UserDefaults.standard.set(vibrationToggle.isOn,
                                forKey: UserDefaultKeys.kVibrationEnabled)
    }
    if let isSoundEnabled = UserDefaults
      .standard.value(forKey: UserDefaultKeys.kSoundEnabled) as? Bool {
      soundToggle.isOn = isSoundEnabled
    } else {
      UserDefaults.standard.set(soundToggle.isOn,
                                forKey: UserDefaultKeys.kSoundEnabled)
    }
    if let selectedSound = UserDefaults
      .standard.value(forKey: UserDefaultKeys.kSelectedSound) as? String {
      if selectedSound == SoundName.accent.rawValue {
        soundChanged(soundBtns[1])
      } else if selectedSound == SoundName.alarm.rawValue {
        soundChanged(soundBtns[2])
      } else {
        soundChanged(soundBtns[0])
      }
    } else {
      UserDefaults.standard.set(SoundName.spaceDrop.rawValue,
                                forKey: UserDefaultKeys.kSelectedSound)
    }
  }
  
  func loadImages() {
    vibrationToggleImageView.image = self.loadImagefromDisk(imageName: MiscellaneousIcons.vibrationToggle.rawValue)
    soundToggleImageView.image = self.loadImagefromDisk(imageName: MiscellaneousIcons.soundToggle.rawValue)
  }
}
