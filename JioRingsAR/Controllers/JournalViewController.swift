//
//  JournalViewController.swift
//  Jio-Covid

import UIKit
import Alamofire

class JournalViewController: UIViewController {

  @IBOutlet weak var outsideTimeLabel: CustomLabel!
  @IBOutlet weak var totalTimeLabel: CustomLabel!
  @IBOutlet weak var avgSafetyLabel: CustomLabel!  
  @IBOutlet weak var totalViolationsLabel: CustomLabel!
  
  @IBOutlet weak var violationsImageView: UIImageView!
  @IBOutlet weak var safetyMaintainedImageView: UIImageView!  
  @IBOutlet weak var timeTrackedImageView: UIImageView!
  @IBOutlet weak var walkingImageView: UIImageView!
  
  @IBOutlet var hrsLabels: [CustomLabel]!
  @IBOutlet weak var graphContainer: UIView!
  
  // MARK: - Variables  
  let graphView = LineChartView(frame: CGRect(x: 16.0,
                                              y: 0,
                                              width: kScreenWidth - 32,
                                              height: 300))
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loadImages()
    refreshData()
  }
  
  func loadImages() {
    violationsImageView.image = self.loadImagefromDisk(imageName: StatsIcons.totalViolations.rawValue)
    safetyMaintainedImageView.image = self.loadImagefromDisk(imageName: StatsIcons.safetyMaintained.rawValue)
    timeTrackedImageView.image = self.loadImagefromDisk(imageName: StatsIcons.timeTracked.rawValue)
    walkingImageView.image = self.loadImagefromDisk(imageName: StatsIcons.walking.rawValue)
  }
  
  func refreshData() {
    getJournalData()
    getGraphData()
  }
  
  func createGraph(yPos: [CGFloat]) {
    graphView.backgroundColor = UIColor.clear
    
    
    let hrsLabelWidth = (kScreenWidth - 32)/5
    
    graphView.points = [CGPoint(x: 1*hrsLabelWidth, y: yPos[0]),
                        CGPoint(x: 2*hrsLabelWidth, y: yPos[1]),
                        CGPoint(x: 3*hrsLabelWidth, y: yPos[2]),
                        CGPoint(x: 4*hrsLabelWidth, y: yPos[3])]
    graphView.setNeedsDisplay()
    self.graphContainer.addSubview(graphView)
  }
  
  func getJournalData() {
    let urlString = baseApiUrl + "/activity/journal"
     
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
     
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let journalResult: Result<BaseJsonStruct<ActivityJournal>> = decoder.decodeResponse(from: response)
        guard let journalData = journalResult.value else {
          return
        }
        self.outsideTimeLabel.text = "0 hrs"
        self.setTotalTime(time: journalData.data.totalDuration)
        self.avgSafetyLabel.text = "\(journalData.data.safetyRate.truncate(places: 2))%"
        self.totalViolationsLabel.text = "\(journalData.data.totalViolations)"
    }
  }
  
  func setTotalTime(time: Int64) {
    let sessionTime = CGFloat(time)
    if sessionTime < 60 {
      self.totalTimeLabel.text = "\(time) sec"
    } else if sessionTime < 3600 {
      let minutes = Int(sessionTime/60.0)
      self.totalTimeLabel.text = "\(minutes) mins"
    } else {
      let hours = Int(sessionTime/3600.0)
      self.totalTimeLabel.text = "\(hours) hrs"
    }
  }
  
  func getGraphData() {
    let urlString = baseApiUrl + "/activity/graph-plot-data?days=1"
     
    let token = UserDefaults.standard.value(forKey: UserDefaultKeys.kAuthToken) as? String ?? ""
     
    Alamofire.request(urlString,
                      method:.get,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: ["Authorization": token])
      .responseData { response in
        let decoder = JSONDecoder()
        let journalResult: Result<BaseJsonStruct<ActivityJournal>> = decoder.decodeResponse(from: response)
        guard let journalData = journalResult.value else {
          return
        }
        self.outsideTimeLabel.text = "0 hrs"
        self.setTotalTime(time: journalData.data.totalDuration)
        self.avgSafetyLabel.text = "\(journalData.data.safetyRate.truncate(places: 2))%"
        self.totalViolationsLabel.text = "\(journalData.data.totalViolations)"
    }
  }
  
  func getGraphYPositions(graph: Graph) {
    var violationsToPlot: [Int] = []
    var prevSessionViolations = 0
    for graphData in graph.data {
      var currentSessionViolations = 0
      for session in graphData.plotDataArray {
        currentSessionViolations += session.violationCount
      }
      prevSessionViolations += currentSessionViolations
      violationsToPlot.append(prevSessionViolations)
    }
    
    var graphPoints: [CGFloat] = []
    var point: CGFloat = 0
    for violation in violationsToPlot {
      if violation <= 40 {
        point = CGFloat(247.0 - CGFloat(violation)*5.55)
      } else {
        point = 28.0
      }
      graphPoints.append(point)
    }
    
    createGraph(yPos: graphPoints)
  }
}
