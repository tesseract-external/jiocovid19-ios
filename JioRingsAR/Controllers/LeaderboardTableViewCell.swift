//
//  LeaderboardTableViewCell.swift
//  Jio-Covid

import UIKit

class LeaderboardTableViewCell: UITableViewCell {

  @IBOutlet weak var userRankLabel: CustomLabel!
  @IBOutlet weak var userNameInitialLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var safetyPercentageLabel: UILabel!

  func setupUI(user: LeaderboardUser, index: Int) {
    self.safetyPercentageLabel.text = "\(user.lastNetScore.truncate(places: 2))%"
    self.userRankLabel.text = "\(index + 1)"
    self.userNameLabel.text = user.fullName?.capitalized ?? ""
    self.userNameInitialLabel.text = user.fullName?.first?.uppercased() ?? ""
    self.durationLabel.text = getTotalDays(createdAt: user.createdAt)
  }
  
  func getTotalDays(createdAt: String) -> String {
    let dateFormatter = DateFormatter()
        
    let startDateString = String(createdAt.dropLast())
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    guard let startDate = dateFormatter.date(from: startDateString) else {
      return "0 days"
    }
    
    let diffInDays = Calendar.current.dateComponents([.day],
                                                     from: startDate,
                                                     to: Date()).day
    
    return "\(diffInDays ?? 0) days"
  }
}
