//
//  NameViewController.swift
//  Jio-Covid
//

import UIKit
import Alamofire
import AVFoundation

class NameViewController: UIViewController {

  // MARK: - IBOutlets
  @IBOutlet weak var jioLogoImageView: UIImageView!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var dismissFlowBtn: UIButton!
  
  // MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    nameTextField.becomeFirstResponder()
    jioLogoImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.jioLogo.rawValue)
    dismissFlowBtn.setImage(self.loadImagefromDisk(imageName: MiscellaneousIcons.dismissCovid.rawValue),
                            for: .normal)
  }
  
  // MARK: - IBActions
  @IBAction func continueDidTap(_ sender: Any) {
    if nameTextField.text != nil {
      if let uuid = UserDefaults.standard.value(forKey: UserDefaultKeys.kUUID) as? String {
        loginUser(name: nameTextField.text ?? "" , uuid: uuid)
      }
    }
  }
  
  @IBAction func dismissCovidDidTap(_ sender: Any) {
    if let navVC = self.navigationController {
      navVC.dismiss(animated: true, completion: nil)
    }
  }
  
  func loginUser(name: String, uuid: String) {
    let urlString = baseApiUrl + "/auth/login"
    
    let parameters: [String: Any] = ["credentials": ["clientId": uuid,
                                                     "fullName": "\(name)"],
                                     "userType": 2,
                                     "strategy": "jio"]
    
    activityIndicator.startAnimating()
    
    Alamofire.request(urlString,
               method:.post,
               parameters: parameters,
               encoding: JSONEncoding.default)
      .responseData { response in
        let decoder = JSONDecoder()
        let userResult: Result<User> = decoder.decodeResponse(from: response)
        self.activityIndicator.stopAnimating()
        guard let user = userResult.value else {
          return
        }
        UserDefaults.standard.set(user.data.id, forKey: UserDefaultKeys.kUserId)
        UserDefaults.standard.set(user.authToken, forKey: UserDefaultKeys.kAuthToken)
        UserDefaults.standard.set(user.data.fullName ?? name, forKey: UserDefaultKeys.kUserName)
        UserDefaults.standard.set(user.data.lastNetScore, forKey: UserDefaultKeys.kUserSafety)
        DispatchQueue.main.async {
          self.moveToPermissions()
        }
    }
  }
  
  func moveToPermissions() {
    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
    
    switch cameraAuthorizationStatus {
    case .authorized:
      self.performSegue(withIdentifier: "showHome", sender: nil)
    default:
      self.performSegue(withIdentifier: "showPermissions", sender: nil)
    }

  }
}
