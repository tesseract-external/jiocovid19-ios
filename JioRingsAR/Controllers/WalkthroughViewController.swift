//
//  WalkthroughViewController.swift
//  Jio-Covid

import UIKit
import AVFoundation
import Alamofire
import CoreML

class WalkthroughViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var getStartedBtn: UIButton!
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var walkthroughOneImageView: UIImageView!  
  @IBOutlet weak var walkthroughTwoImageView: UIImageView!
  
  @IBOutlet weak var dismissFlowBtn: UIButton!
  @IBOutlet weak var scroller: UIScrollView!
  
  // MARK: - Variables
  var uniqueId = ""
  
  // MARK: - Lifecycle Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    self.scroller.delegate = self
    if let uuid = UserDefaults.standard.value(forKey: UserDefaultKeys.kUUID) as? String {
      uniqueId = uuid
    } else {
      uniqueId = UUID().uuidString
      UserDefaults.standard.set(uniqueId, forKey: UserDefaultKeys.kUUID)
    }
    
    if let userName = UserDefaults.standard.value(forKey: UserDefaultKeys.kUserName) as? String,
      let uuid = UserDefaults.standard.value(forKey: UserDefaultKeys.kUUID) as? String {
      loginUser(name: userName,
                uuid: uuid)
    }
    loadImages()
  }
  
  @IBAction func getStartedBtnTapped(_ sender: Any) {   
    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
    
    switch cameraAuthorizationStatus {
    case .authorized:
      if UserDefaults.standard.value(forKey: UserDefaultKeys.kUserName) as? String != nil {
        moveToHome()
      } else {
        moveToNameController()
      }
    default:
      if UserDefaults.standard.value(forKey: UserDefaultKeys.kUserName) as? String != nil {
        moveToPermissions()
      } else {
        moveToNameController()
      }
    }
  }
  
  @IBAction func dismissCovidDidTap(_ sender: Any) {
    if let navVC = self.navigationController {
      navVC.dismiss(animated: true, completion: nil)
    }
  }
  
  func loadImages() {
    walkthroughOneImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.walkthrough1.rawValue)
    walkthroughTwoImageView.image = self.loadImagefromDisk(imageName: OnboardingImages.walkthrough2.rawValue)
  }
  
  func moveToHome() {
    self.performSegue(withIdentifier: "showHome", sender: nil)
  }
  
  func moveToPermissions() {
    self.performSegue(withIdentifier: "showPermissions", sender: nil)
  }
  
  func moveToNameController() {
    self.performSegue(withIdentifier: "showNameController", sender: nil)
  }
  
  func loginUser(name: String, uuid: String) {
    let urlString = baseApiUrl + "/auth/login"
    
    let parameters: [String: Any] = ["credentials": ["clientId": uuid,
                                                     "fullName": "\(name)"],
                                     "userType": 2,
                                     "strategy": "jio"]
    
    Alamofire.request(urlString,
             method:.post,
             parameters: parameters,
             encoding: JSONEncoding.default)
    .responseData { response in
      let decoder = JSONDecoder()
      let userResult: Result<User> = decoder.decodeResponse(from: response)
      guard let user = userResult.value else {
        return
      }
      UserDefaults.standard.set(user.data.id, forKey: UserDefaultKeys.kUserId)
      UserDefaults.standard.set(user.authToken, forKey: UserDefaultKeys.kAuthToken)
      UserDefaults.standard.set(user.data.fullName, forKey: UserDefaultKeys.kUserName)
      UserDefaults.standard.set(user.data.lastNetScore, forKey: UserDefaultKeys.kUserSafety)
    }
  }
}

extension WalkthroughViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let currentPage = scrollView.contentOffset.x / kScreenWidth
    self.pageControl.currentPage = Int(currentPage)
  }
}
