//
//  JioCovid.swift
//  JioCovid
//

import Foundation
import UIKit
import ARKit

public class JioRingsARObject {
  
  public init() {}
  
  public class func isDeviceSupported() -> Bool {
    if #available(iOS 13.0, *) {
      return true
    } else {
      return false
    }
  }

  public class func presentJioRings(uuid: String) {
    UserDefaults.standard.set(uuid, forKey: UserDefaultKeys.kUUID)
    
    let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    
    if var topController = keyWindow?.rootViewController {
      while let presentedViewController = topController.presentedViewController {
        topController = presentedViewController
      }
      
      let navController = UINavigationController()
      navController.isNavigationBarHidden = true
      navController.modalPresentationStyle = .overFullScreen
      let rootViewController = createRootViewController()
      navController.setViewControllers([rootViewController],
                                       animated: false)
            
      topController.present(navController, animated: true, completion: nil)
    }
  }

  class func createRootViewController() -> UIViewController {
    let bundle = Bundle(for: JioRingsARObject.self)
    let storyboard = UIStoryboard(name: "Main", bundle: bundle)
    if #available(iOS 13.0, *) {
      if areAssetsMissing() {
        return storyboard.instantiateViewController(withIdentifier: "DownloadViewController")
      } else {
        return storyboard.instantiateViewController(withIdentifier: "HomeViewController")
      }
    } else {
      return storyboard.instantiateViewController(withIdentifier: "DownloadViewController")
    }
  }
  
  class func areAssetsMissing() -> Bool {
    let imageFilePath = FileManager.documentDirectoryURL.appendingPathComponent("Images")
    if !FileManager.default.fileExists(atPath: imageFilePath.path) {
      return true
    }
    let ringFilePath = FileManager.documentDirectoryURL.appendingPathComponent("coloredRings.usdz")
    if #available(iOS 13.0, *) {
      if !FileManager.default.fileExists(atPath: ringFilePath.path) && !ARBodyTrackingConfiguration.isSupported {
        return true
      }
    } else {
      return false
    }
    
    if UserDefaults.standard.value(forKey: UserDefaultKeys.kMLModelUrl) as? String == nil {
      if #available(iOS 13.0, *), !ARBodyTrackingConfiguration.isSupported {
        return true
      }
    }
    return false
  }
}
