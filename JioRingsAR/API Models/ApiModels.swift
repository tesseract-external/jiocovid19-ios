//
//  User.swift
//  Jio-Covid
//

import Foundation

struct BaseJsonStruct<T:Decodable> : Decodable {    
  let data: T
  
  enum CodingKeys: String, CodingKey {
    case data = "data"    
  }
}

// User
struct User: Decodable {
  let statusCode: Int
  let data: UserData
  let authToken: String
  
  enum CodingKeys: String, CodingKey {
    case statusCode
    case data
    case authToken
  }
}

struct UserData: Decodable {
  let id: String
  let lastActiveLocation: Location?
  let preferences: Preferences
  let lastActiveTime: String
  let lastNetScore: Double
  let fullName: String?
  let phone: Int64?
  let createdAt: String
  let updatedAt: String
  let lastLogin: String
  let clientId: String
  let imageUrl: String?
    
  enum CodingKeys: String, CodingKey {
    case id = "id"
    case lastActiveLocation = "lastActiveLocation"
    case preferences = "preferences"
    case lastActiveTime = "lastActiveTime"
    case lastNetScore = "lastNetScore"
    case fullName = "fullName"
    case phone = "phone"    
    case createdAt = "createdAt"
    case updatedAt = "updatedAt"
    case lastLogin = "lastLogin"
    case clientId = "clientId"
    case imageUrl = "imageUrl"
  }
}

struct Location: Decodable {
  let latitude: Double
  let longitude: Double
  
  enum CodingKeys: String, CodingKey {
    case latitude = "latitude"
    case longitude = "longitude"
  }
}

struct Preferences: Decodable {
  let notification: Bool
  let vibration: Bool
  let sound: Bool
  
  enum CodingKeys: String, CodingKey {
    case notification = "notification"
    case vibration = "vibration"
    case sound = "sound"
  }
}

// Activity
struct Activity: Decodable {
  let activityData: ActivityData
  let rank: Int64
  let nearMeRank: Int64?
  let score: Double
    
  enum CodingKeys: String, CodingKey {
    case activityData = "data"
    case rank
    case nearMeRank
    case score
  }
}

struct ActivityData: Decodable {
  let id: String
  let startTime: String
  let user: String
  let totalDuration: Int64
  let violationCount: Int
  let safetyRate: Double
  let createdAt: String
  let updatedAt: String
    
  enum CodingKeys: String, CodingKey {
    case id
    case startTime
    case user
    case totalDuration
    case violationCount
    case safetyRate
    case createdAt
    case updatedAt    
        
  }
}

struct ActivityJournal: Decodable {
  let totalDuration: Int64
  let totalViolations: Int
  let safetyRate: Double
    
  enum CodingKeys: String, CodingKey {
    case totalDuration = "totalDuration"
    case totalViolations = "totalViolations"
    case safetyRate = "safetyRate"
  }
}

// Leaderboard
struct Leaderboard: Decodable {
  let leaderboardUsers: [LeaderboardUser]
  let count: Int
  let next: Int?
  let totalCount: Int64
  let prev: Int?
    
  enum CodingKeys: String, CodingKey {
    case leaderboardUsers = "result"
    case count = "count"
    case next = "next"
    case totalCount = "totalCount"
    case prev = "prev"
  }
}

struct LeaderboardUser: Decodable {
  let id: String
  let fullName: String?
  let imageUrl: String?
  let lastNetScore: Double
  let createdAt: String
    
  enum CodingKeys: String, CodingKey {
    case id
    case fullName
    case imageUrl
    case lastNetScore
    case createdAt
  }
}

struct GlobalRank: Decodable {
  let rank: Int64
    
  enum CodingKeys: String, CodingKey {
    case rank = "data"
  }
}

// Graph
struct Graph: Decodable {
  let data: [GraphData]
    
  enum CodingKeys: String, CodingKey {
    case data
  }
}

struct GraphData: Decodable {
  let plotDataArray: [PlotData]
  let withinDuration: Int
    
  enum CodingKeys: String, CodingKey {
    case plotDataArray = "plotdata"
    case withinDuration
  }
}

struct PlotData: Decodable {
  let violationCount: Int
  let createdAt: String
    
  enum CodingKeys: String, CodingKey {
    case violationCount
    case createdAt
  }
}
